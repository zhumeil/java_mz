#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R1.5 The following are all benefits to using Java over machine code:
1/ Java bytecode is platform independent and may be run on any system that has a JVM. This quality is known as portability.
2/ Java JIT compilers can run efficient Java programs as fast or faster than machine code programs
3/ Java is memory-managed. This reduces about 95% of runtime errors (mostly memory-related) as compared to unmanaged code.
4/ Java was designed for networking and the Internet in mind; and it is generally safer than machine code
5/ With the introduction of Java8, Java is now a declarative and functional programming language.

R2.4 Translating math expressions to Java:
a/ s = s + v * t + (1/2) * g * Math.pow(t, 2)
b/ G = 4 * Math.pow(Math.PI, 2) * (Math.pow(a, 3)/(Math.pow(p, 2) * (m1 * m2))
c/ FV = PV * Math.pow(1 + (INT/100.0), YRS)
d/ c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2) - 2 * a * b * Math.cos(y))

R2.7 Understanding integer operations, order of precedence, and integer division:
n is 17 and m is 18
a. n / 10 + n % 10 = 1
b. n % 2 + m % 2 = 1
c. (m + n) / 2 = 17
d. (m + n) / 2.0 = 17.5
e. (int) (0.5 * (m + n)) = 17
f. (int) Math.round(0.5 * (m + n)) = 18


R2.14 How the compiler interprets primitive values
Explain the differences between 2, 2.0, '2', "2", and "2.0".
2 is an integer which has no fractional parts
2.0 is a double which has fractional parts
'2' is a char and cannot be evaluated as a string
"2" and "2.0" are both strings and cannot be evaluated as an int or double


R2.17 Pseudocode understanding
Write pseudocode for a program that reads a name (such as Harold James Morgan) and then prints a monogram consisting of the initial letters of the first, middle, and last name (such as HJM).
String name = "Harold James Morgan"
Char first = char at position 0
Char middle = char at position 7
Char last = char at position 13
Print (first + middle + last)


R2.22 More pseudocode - you do not need to draw a diagram for this question
The following pseudocode describes how to obtain the name of a day, given the day number
(0 = Sunday, 1 = Monday, and so on.)
    Declare a string called names containing "SunMonTueWedThuFriSat".
    Compute the starting position as 3 x the day number.
    Extract the substring of names at the starting position with length 3.
Given the day number 4, the starting position would be 12, which is 'h' and the sub string following that would be "huF".
The count starts at zero so we should actually subtract 1 from the starting position with 11 being the correct starting position.

R3.19 Pseudocode for grade problem
if score is less than or equal to 100 or greater than or equal to 90
grade is an A
else if score is less than or equal to 89 or greater than or equal to 80
grade is a B
else if score is less than or equal to 79 or greater than or equal to 70
grade is a C
else if score is less than or equal to 69 or greater than or equal to 60
grade is a D
else grade is an F


R3.30 Boolean expressions
b is false and x is 0
a. b && x == 0 False
b. b || x == 0 True
c. !b && x == 0 True
d. !b || x == 0 True
e. b && x != 0 False
f. b || x != 0 False
g. !b && x != 0 False
h. !b || x != 0 True

R4.12 Implementing loops
Which loop statements does Java support? Give simple rules for when to use each loop type.
1/ While loop has a given a boolean statement, if statement is true, code executes. The boolean statement must should be updated so that eventually the boolean statement is false and the loop is stopped.
2/ For loops groups the initialization, testing condition, and update to the testing condition together. All these elements need to be related and the compiler won't check for that. You should also not update the counter in the body of the for loop because the count gets incremented twice in this case.
3/ Do while loop executes the body of the loop first and then check the test condition. This means the do while loop will always execute at least once before the condition is checked.

R4.14 Calendar problem
print "Su  M  T  W  Th  F Sa"
day = 1
while day <= 31
if day = 1
    print day + "     "
if day is 4, 11, 18, or 25,
    print day + " " on new line
else
    print day + " "
day ++ 1



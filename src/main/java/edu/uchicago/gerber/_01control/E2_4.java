package edu.uchicago.gerber._01control;
import java.util.Scanner;
/**
 * MPCS 51036
 * Meilin Zhu
 * E2.4 Print integers from CLI
 * */
public class E2_4 {
    public static void main(String[] args) {

        // We get user input for integers a and b
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter two integers: ");
        int a = in.nextInt();
        int b = in.nextInt();

        // Calculate the values of each
        int sum = a + b;
        int diff = a - b;
        int prod = a * b;
        int avg = (a + b) / 2;
        int dist = Math.abs(diff);
        int max = Math.max(a, b);
        int min = Math.min(a, b);

        // Print values
        System.out.println("Sum: " + sum);
        System.out.println("Difference: " + diff );
        System.out.println("Product: "+ prod);
        System.out.println("Average: " +  avg);
        System.out.println("Distance: "+ dist);
        System.out.println("Max: " + max);
        System.out.println("Min: " + min);

    }
}

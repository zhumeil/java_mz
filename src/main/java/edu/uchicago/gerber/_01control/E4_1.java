package edu.uchicago.gerber._01control;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * E4.1 More loops
 * */
public class E4_1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        /** a.  The sum of all even numbers between 2 and 100 (inclusive).*/
        int asum = 0;
        int even = 2;
        // Condition to end loop is when we reach 100
        while(even <= 100){
            // increment sum
            asum = asum + even;
            // increment even integer
            even = even + 2;
        }
        System.out.println("a. The sum of all even numbers between 2 and 100 inclusive is " + asum);

        /** b. The sum of all squares between 1 and 100 (inclusive).*/
        int sqr = 1;
        int bsum = 0;
        int count = 1;
        // Condition to end loop is when we reach 100 as perfect square so 10 for count
        while(sqr <= 100){
            //Increment sum
            bsum = bsum + sqr;
            // Increment perfect square
            sqr = count * count;
            // Increment count
            count ++;
        }
        System.out.println("b. The sum of all squares between 1 and 100 inclusive is " + bsum);

        /**c. All powers of 2 from 2^(0) up to 2^(20).*/
        System.out.println("All powers of 2 from 2^(0) up to 2^(20)");
        // We find the value of the power of 2 with exponents from 0 to 20
        for(int i = 0; i <=20; i++){
            double power = Math.pow(2, i);
            System.out.println(power);
        }

        /** d. The sum of all odd numbers between a and b (inclusive), where a and b are inputs.*/

        System.out.print("Please enter two integers: ");
        int a = in.nextInt();
        int b = in.nextInt();
        int dsum = 0;
        int odd = 0;

        // Determine if starting value is odd
        // If even, we add one to make it odd
        if(a%2 == 0){
            odd = a + 1;
        }
        else{
            odd = a;
        }

        // While the count is less than the ending value, we increment the sum by the next odd number
        while(odd <= b){
            dsum = dsum + odd;
            odd = odd + 2;
        }
        System.out.println("The sum of all odd numbers between " + a + " and " +b + " is " + dsum);

        /**e. The sum of all odd digits of an input. (For example, if the input is 32677, the sumwouldbe3+7+7=17.)*/
        System.out.print("Please enter a integer: ");
        // Take input as String to parse substring
        String input = in.nextLine();
        int esum = 0;

        for(int i = 0; i < input.length(); i ++){
            // We find the number at each index by converting the String to int
            int num = Integer.parseInt(input.substring(i, i+1));
            if (num % 2 != 0){
                esum = esum + num;
            }
        }

        System.out.print("The sum of all odd digits of " + input + " is " + esum);

    }
}

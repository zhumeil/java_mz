package edu.uchicago.gerber._01control;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P2.5 Dollars and cents extraction
 * */

public class P2_5 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter the price:");
        double price = in.nextDouble();
        // Dollar value is the non-fractional value of the price
        int dollar = (int) price;

        // The cent is the difference between the double and integer values
        double cents = (price - dollar) * 100 + 0.5;
        System.out.println("Value is " + dollar + " dollars and " + (int)cents + " cents");
    }
}

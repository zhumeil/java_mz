package edu.uchicago.gerber._01control;
import java.util.Scanner;
/**
 * MPCS 51036
 * Meilin Zhu
 * E3.14 The seasons program
 * */
public class E3_14 {

    public static void main(String[] args) {

        // Get user input for month and day as numbers
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the month and day in numerical form:");
        int month = in.nextInt();
        int day = in.nextInt();

        // Initialize variable season
        String season = null;

        // If statements to determine season based on value of month
        if(month == 1 || month == 2 || month == 3) {
            season = "Winter";
        }
        else if(month == 4 || month == 5 || month == 6){
            season = "Spring";
        }
        else if(month == 7 || month == 8 || month == 9){
            season = "Summer";
        }
        else if(month == 10 || month == 11 || month == 12){
            season = "Fall";
        }

        // Additional if statements that overrides earlier statements if applicable
        if(month%3 == 0 && day > 21){
            if(season.equals("Winter")){
                season = " Spring";
            }
            else if(season.equals("Spring")){
                season = " Summer";
            }
            else if(season.equals("Summer")){
                season = " Fall";
            }
            else season = "Winter";
        }

        // Print
        System.out.println("The season is " + season);
    }

}

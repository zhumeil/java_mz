package edu.uchicago.gerber._01control;

import java.util.Scanner;
/**
 * MPCS 51036
 * Meilin Zhu
 * E.4.27 Digital to binary
 * */

/**
 * Write a program that reads a number and prints all of its binary digits:
 * Print the remainder number % 2, then replace the number with number /2.
 * Keep going until the number is 0.
 * For example, if the user provides the input 13, the output should be
 * 1
 * 0
 * 1
 * 1*/

public class E4_17 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Please enter a number: ");
        int input = in.nextInt();
        int in_cal = input;
        String bi = "";

        // Here we are calculating the value manually
        while (in_cal > 0){
            // The output prints vertically
            System.out.println(in_cal%2);
            // The input is decremented by 1/2
            in_cal = in_cal/2;
        }
        // Built-in function for finding binary prints value from right to left
        System.out.println("The binary value is "  + Integer.toBinaryString(input));
    }
}

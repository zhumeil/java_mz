package edu.uchicago.gerber._01control;

/**
 * MPCS 51036
 * Meilin Zhu
 * E2.20 Ascii graphics - x-mas tree
 * */

public class E2_20 {

    public static void main(String[] args) {

        // Use '\\' to print '\'
        // \"\" to print double quotes
        System.out.print(
                "   /\\' \n"+
                "  /  \\' \n" +
                " /    \\' \n" +
                "/      \\' \n" +
                "-------- \n " +
                " \"  \" \n  \"  \" \n  \"  \" ");

    }

}

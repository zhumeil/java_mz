package edu.uchicago.gerber._01control;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P3.13 Roman numeral conversion
 * */

public class P3_13 {

    public static void main(String[] args) {

        // Initialized String variable for each decimal place
        String thousands = null;
        String hundreds = null;
        String tens = null;
        String ones = null;

        // Initialized int variable for each decimal place
        int t = 0;
        int h = 0;
        int tn = 0;
        int o = 0;

        Scanner in = new Scanner(System.in);
        System.out.print("Please enter a year: ");
        int num = in.nextInt();

        // If input is grater than 3999, we end program
        if (num > 3999){
            System.out.println("Only numbers up to 3,999 are represented.");
        }
        else{

            // Convert input to string to determine length
            String numStr = String.valueOf(num);
            int count = numStr.length();
            // We assign values to each decimal place depending on the count
            if (count == 4) {
                t = Integer.valueOf(numStr.substring(0,1));
                h = Integer.valueOf(numStr.substring(1,2));
                tn = Integer.valueOf(numStr.substring(2,3));
                o = Integer.valueOf(numStr.substring(3,4));
            }
            else if (count == 3) {
                h = Integer.valueOf(numStr.substring(0,1));
                tn = Integer.valueOf(numStr.substring(1,2));
                o = Integer.valueOf(numStr.substring(2,3));
            }
            else if (count == 2) {
                tn = Integer.valueOf(numStr.substring(0,1));
                o = Integer.valueOf(numStr.substring(1,2));
            }
            else if (count == 1) {
                o = Integer.valueOf(numStr.substring(0,1));
            }

            // Switch statement for each decimal place
            //Thousands
            switch(t){
                case 1:
                    thousands = "M";
                    break;
                case 2:
                    thousands = "MM";
                    break;
                case 3:
                    thousands = "MMM";
                    break;
            }
            //Hundreds
            switch (h){
                case 1:
                    hundreds = "C";
                    break;
                case 2:
                    hundreds = "CC";
                    break;
                case 3:
                    hundreds = "CCC";
                    break;
                case 4:
                    hundreds = "CD";
                    break;
                case 5:
                    hundreds = "D";
                    break;
                case 6:
                    hundreds = "DC";
                    break;
                case 7:
                    hundreds = "DCC";
                    break;
                case 8:
                    hundreds = "DCCC";
                    break;
                case 9:
                    hundreds = "CM";
                    break;

            }
            //Tens
            switch (tn){
                case 1:
                    tens = "X";
                    break;
                case 2:
                    tens = "XX";
                    break;
                case 3:
                    tens = "XXX";
                    break;
                case 4:
                    tens = "XL";
                    break;
                case 5:
                    tens = "L";
                    break;
                case 6:
                    tens = "LX";
                    break;
                case 7:
                    tens = "LXX";
                    break;
                case 8:
                    tens = "LXXX";
                    break;
                case 9:
                    tens = "XC";
                    break;
            }
            // Ones
            switch (o){
                case 1:
                    ones = "I";
                    break;
                case 2:
                    ones = "II";
                    break;
                case 3:
                    ones = "III";
                    break;
                case 4:
                    ones = "IV";
                    break;
                case 5:
                    ones = "V";
                    break;
                case 6:
                    ones = "VI";
                    break;
                case 7:
                    ones = "VII";
                    break;
                case 8:
                    ones = "VIII";
                    break;
                case 9:
                    ones = "IX";
                    break;

            }

        }
        // Print final answer by concatenating all the String values of each decimal place
        System.out.println("The roman numeral is:" + thousands + hundreds + tens + ones);
    }
}

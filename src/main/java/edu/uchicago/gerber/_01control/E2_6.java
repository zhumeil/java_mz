package edu.uchicago.gerber._01control;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * E2.6 Conversions imperial to metric
 * */

public class E2_6 {
    public static void main(String[] args) {

        // Get user input for measurement in meter
        // Measurement value is a double
        Scanner in = new Scanner(System.in);
        System.out.print("Please enter a measurement in meter: ");
        double meters = in.nextFloat();

        // Convert meters to miles, feet, and inches
        double miles = meters / 1609.34;
        double feet = meters * 3.28;
        double inches = meters * 39.37;

        // Print conversions
        System.out.println("Miles: " + miles);
        System.out.println("Feet: " + feet );
        System.out.println("Inches: "+ inches);

    }
}

package edu.uchicago.gerber._01control;
import java.util.Scanner;
/**
 * MPCS 51036
 * Meilin Zhu
 * E3.7 Simple income tax

 The original U.S. income tax of 1913 was quite simple.
 * The tax was
 * •  1 percent on the first $50,000.
 * •  2 percent on the amount over $50,000 up to $75,000.
 * •  3 percent on the amount over $75,000 up to $100,000.
 * •  4 percent on the amount over $100,000 up to $250,000.
 * •  5 percent on the amount over $250,000 up to $500,000.
 * •  6 percent on the amount over $500,000.There was no separate schedule for single or married taxpayers.
 * Write a program that computes the income tax according to this schedule.*/

public class P3_7 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Enter annual income: ");
        double income = in.nextDouble();
        double tax = 0;

        // Created if statements based on the parameters given
        if (income == 50000){
            tax = 0.01 * income;
        }
        else if (income > 50000 || income <= 75000){
            tax = 0.02 * income;
        }
        else if(income > 75000 || income <= 100000){
            tax = 0.03 * income;
        }
        else if(income > 100000 || income <= 250000){
            tax = 0.04 * income;
        }
        else if(income > 250000 || income <= 500000){
            tax = 0.05 * income;
        }
        else if (income > 500000) {
            tax = 0.06 * income;
        }
        else {
            System.out.println("If your income is less than $50,000, there is no tax!");
        }

        System.out.println("The tax amount is " + tax);


    }
}

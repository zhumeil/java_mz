package edu.uchicago.gerber._01control;

import java.util.Scanner;
/**
 * MPCS 51036
 * Meilin Zhu
 * P3.14 Leap year
 * A year with 366 days is called a leap year. Leap years are necessary to keep the calendar synchronized with the sun because the earth revolves around the sun once every 365.25 days. Actually, that figure is not entirely precise, and for all dates after 1582 the Gregorian correction applies. Usually years that are divisible by 4 are leap years, for example 1996. However, years that are divisible by 100 (for example, 1900) are not leap years, but years that are divisible by 400 are leap years (for exam ple, 2000). Write a program that asks the user for a year and computes whether that year is a leap year. Use a single if statement and Boolean operators.••• P3.15
 * */

public class P3_14 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Please enter a year: ");

        int year = in.nextInt();
        boolean isLeap = false;

        // Single if statement for
        // If the year is divisible by 4 or 400 AND if the year is not divisible by 100 or is divisible by 400
        if ((year % 4 == 0 || year % 400 == 0 )&& ( year % 100 != 0 || year % 400  == 0) ){
            isLeap = true;
        }

        System.out.println("The year "+ year + " is a leap year: " + isLeap);
    }
}


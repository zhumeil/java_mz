package edu.uchicago.gerber._06design.E12_4;

import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * E12.4 The program tests addition and subtraction.
 * */
public class Driver {

    public static void main(String[] args) {

        MathProgram mathProgram = new MathProgram();

        Scanner in = new Scanner(System.in);
        System.out.println("Hello, this is your math tutor.\nLet's get started!\n");

        while (true) {

            // The player gets up to two tries per problem.
            int tries = 2;

            // Advance from one level to the next when the player has achieved a score of five points
            if (mathProgram.getScore() == 5 && mathProgram.getLevel() == 3) {
                System.out.println("\nCongrats! You've completed the program");
                break;
            }else if (mathProgram.getScore() == 5){
                System.out.println("\nAdvancing to the next level!");
                mathProgram.setScore(0);
                mathProgram.setLevel(mathProgram.getLevel() + 1);
            }

            int score = mathProgram.getScore();

            System.out.println("\nL" + mathProgram.getLevel() + ": " + mathProgram.getQuestion(mathProgram.getLevel()));

            while (tries > 0) {

                System.out.println("Your answer: ");
                int answer = in.nextInt();

                if (mathProgram.checkAnswer(answer)) {
                    mathProgram.setScore(score + 1);
                    System.out.println("CORRECT! Score: " + mathProgram.getScore());
                    break;
                }
                else {
                    tries--;
                    switch (tries){
                        case 0:
                            System.out.println("INCORRECT! Here's another question");
                            break;
                        case 1:
                            System.out.println("INCORRECT! You have " + tries + " try left");
                            break;
                    }
                }
            }

        }
    }
}

package edu.uchicago.gerber._06design.E12_4;

public class MathProgram {

    private Questions questions = new Questions();
    private int level;
    private int score;

    // Initialize program with level 1 and score of 0
    public MathProgram (){
        level = 1;
        score = 0;
    }

    public String getQuestion (int level) {
        String q = "";
        switch (level) {
            case 1:
                q = questions.getLevelOne();
                break;
            case 2:
                q = questions.getLevelTwo();
                break;
            case 3:
                q = questions.getLevelThree();
                break;
        }
        return q;
    }

    public boolean checkAnswer(int answer){
        boolean correct = false;
        if(questions.getCorrectAnswer() == answer){
            score ++;
            correct = true;
        }
        return correct;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

}

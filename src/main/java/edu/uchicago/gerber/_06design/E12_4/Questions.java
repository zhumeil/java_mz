package edu.uchicago.gerber._06design.E12_4;

import java.util.Random;

public class Questions {

    private String question;
    private int answer;
    private Random r = new Random();

    public Questions(){
        this.question = getLevelOne();
        this.question = getLevelTwo();
        this.question = getLevelThree();
    }

    public int getCorrectAnswer(){
        return answer;
    }

    // In level 1, it tests only addition of numbers less than ten whose sum is less than ten.
    public String getLevelOne(){

        while(true){
            int a = r.nextInt(9);
            int b = r.nextInt(9);

            if (a + b < 10) {
                question = a + " + " + b;
                answer = a + b;
                break;
            }
        }
        return question;
    }

    // In level 2, it tests addition of arbitrary one-digit numbers.
    public String getLevelTwo(){
        int c = r.nextInt(9);
        int d = r.nextInt(9);
        question = c + " + " + d;
        answer = c + d;
        return question;
    }

    // In level 3, it tests subtraction of one-digit numbers with a non-negative difference.
    public String getLevelThree(){
        while(true){
            int e = r.nextInt(9);
            int f = r.nextInt(9);

            if (e - f > 0){
                question = e + " - " + f;
                answer = e - f;
            }
                break;
            }
        return question;

        }


}


package edu.uchicago.gerber._06design.P12_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P12.1 Vending Machine
 */

public class Driver {

    private static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {

        VendingMachine vendingMachine = new VendingMachine();

        // adding items
        List<Product> products = new ArrayList<>();
        products.add(new Product("Snickers", 1.75, 5));
        products.add(new Product("Twix", 1.75,5));
        products.add(new Product("Reeses", 1.55, 5));
        products.add(new Product("Pop Tart", 1.25, 6));
        products.add(new Product("Skittles", 0.75, 10));
        products.add(new Product("Crunch", 1.75, 2));
        products.add(new Product("Hersheys", 1.50, 5));
        products.add(new Product("Cheez-It", 1.75, 1));
        products.add(new Product("Twizzlers", 0.50, 6));
        products.add(new Product("Pretzle", 1.25, 5));
        vendingMachine.stockMachine(products);


        System.out.println("[VENDING MACHINE]");



        while (true) {

            System.out.println("Type 'i' for insert coins or 's' for select product or type 'exit' to quit");
            String selection = in.nextLine().toLowerCase();
            switch (selection){
                case "i":
                    String moneyAdded = addMoney();
                    vendingMachine.insertCoins(moneyAdded);
                    System.out.println("Balance: " + vendingMachine.returnBalance());
                    break;
                case "s":
                    System.out.println(vendingMachine.showInventory());
                    String productSelection = makeSelection();
                    System.out.println(vendingMachine.vend(productSelection));
                    System.out.println("Balance: " + vendingMachine.returnBalance());
                    break;
                case "money":
                    System.out.println("Clearing machine balance!" );
                    List<Coin> cashOut = vendingMachine.cashOut();
                    for(Coin c : cashOut){
                        System.out.println(c);
                    }
                case "exit":
                    break;
            }


        }

    }

    // copied methods from lab for adding money and making selection
    private static String addMoney(){

        System.out.println("Insert Coins like so: q q d d n");
        return in.nextLine().toLowerCase();

    }

    private static String makeSelection(){
        System.out.println("Please choose a product such as 'A2'");
        return in.nextLine().toUpperCase();

    }
}
package edu.uchicago.gerber._06design.P12_1;

public class Product {

    private String name;
    private double price;
    private int inventory;

   public Product(String n, double p, int i ){
        name = n;
        price = p;
        inventory = i;
   }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getInventory() {
        return inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }

    @Override
    public String toString() {
        return  name + " " + VendingMachine.dollarFormat.format(price) + " [" + inventory +" In stock] \n";

    }
}

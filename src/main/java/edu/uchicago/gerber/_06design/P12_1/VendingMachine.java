package edu.uchicago.gerber._06design.P12_1;

import java.text.DecimalFormat;
import java.util.*;



/**
 * Products can be purchased by inserting coins with a value at least equal to the cost of the product.
 * A user selects a product from a list of available products, adds coins, and either gets the product or gets the coins returned.
 * The coins are returned if insufficient money was supplied or if the product is sold out.
 * The machine does not give change if too much money was added.
 * Products can be restocked and money removed by an operator.
 *
 * */

public class VendingMachine {

    // insert coin
    // return coins
    // remove money
    // restock goods
    // check stock
    public static final DecimalFormat dollarFormat = new DecimalFormat("$0.00");
    private List<Coin> totalBalance;
    private List<Coin> purchaseBalance;
    private String moneyAdded;
    private Map<String, Product> productMap;

    public VendingMachine(){
        totalBalance = new ArrayList<>();
        purchaseBalance = new ArrayList<>();
    }

    // Find product in map and add to inventory
    // For each Product in productList, add to product map and add key
    public void stockMachine(List<Product> productsList){
        productMap = new HashMap<>();

        for(int i = 0; i<productsList.size(); i++){
            productMap.put(getId(i), productsList.get(i));
        }
    }

    // Iterate through hashmap and print product
    public String showInventory(){
        StringBuilder stb = new StringBuilder();
        Product product;
        for (String key : productMap.keySet()) {
            product = productMap.get(key);
            stb.append(key + " : " + product.toString());
                if (product.getInventory() == 0) {
                    stb.append("OUT OF STOCK");
                }
        }
        return stb.toString();
    }

    // convert string to list of values
    public void insertCoins(String strCoins){
        moneyAdded = strCoins;
        String[] c = strCoins.split(" ");
        for (String strCoin : c) {
             Coin coinValue = new Coin(strCoin);
             purchaseBalance.add(coinValue);
        }

    }

    // return balance
    public String returnBalance(){
        double balance = 0.0;
        for (Coin c : purchaseBalance) {
            balance += c.getValue();
        }

        return dollarFormat.format(balance);
    }

    // return total balance as list
    public List<Coin> cashOut(){
        List<Coin> temp = totalBalance;
        //reset the bank
        totalBalance = new ArrayList<>();
        //get the entire bank
        return temp;
    }

    public double getValue(List<Coin> coinList){
        double value = 0.0;
        for (Coin c : coinList) {
            value += c.getValue();
        }
        return value;
    }

    // Convert value of difference to coins and replace purchaseBalance so difference can be appended
    public void returnCoin(double amount){
        purchaseBalance.clear();
        while(true) {
            if (amount - 0.25 >= 0) {
                purchaseBalance.add(new Coin(0.25));
                amount -= 0.25;
            } else if (amount - 0.10 >= 0) {
                purchaseBalance.add(new Coin(0.10));
                amount -= 0.10;
            } else if (amount - 0.05 >= 0) {
                purchaseBalance.add(new Coin(0.05));
                amount -= 0.05;
            } else if (amount - 0.01 >= 0) {
                purchaseBalance.add(new Coin(0.01));
                amount -= 0.01;
            }else
                break;
        }
    }

    // match key to product
    // check inventory
        // if no inventory return funds
        // else vend
    public String vend(String key) {
        String output ="";
        Product product = productMap.get(key);
        double price = product.getPrice();
        double balance = getValue(purchaseBalance);
        if (product.getInventory() == 0) {
            purchaseBalance.clear();
            output =  "Sorry item is out of stock, here's your money back! \n" + moneyAdded;
        } else if (price > balance) {
            purchaseBalance.clear();
            output =  "Sorry, insufficient funds, here's your money back! \n" + moneyAdded;
        } else if (balance > price) {
            double difference = balance - price;
            returnCoin(difference);
            product.setInventory(product.getInventory() - 1);
            appendBalance();
            output = "Enjoy your " + product.getName() + "!";
        }
        return output;
    }


    public void appendBalance(){
        totalBalance.addAll(purchaseBalance);
    }

    public String getId(int i ){
        i++;
        String id ="";
        if (i == 0 || i <= 4 ){
            id = "A" + i;
        }
        else if (i == 5 || i <= 8 ){
            id = "B" + i;
        }
        else
            id = "C" + i;
        return id;
    }


}

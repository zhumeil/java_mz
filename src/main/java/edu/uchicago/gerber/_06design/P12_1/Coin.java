package edu.uchicago.gerber._06design.P12_1;

public class Coin {

    private double value;

    public Coin(double v) {
        value = v;
    }

    public Coin(String strValue){
        switch (strValue){
            case"q": value = 0.25; break;
            case"d": value = 0.10; break;
            case"n": value = 0.05; break;
            case"p": value = 0.01; break;
        }

        setValue(value);
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString(){
        return VendingMachine.dollarFormat.format(getValue());
    }

}

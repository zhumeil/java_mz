package edu.uchicago.gerber._02arrays;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P5.24 Roman numeral
 * */

public class P5_24 {
    public static void main(String[] args) {
        // Prompt user for Roman numeral
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the Roman numeral: ");
        String r = in.nextLine();

        //Initialize array list
        ArrayList<String> romans = new ArrayList<String>();
        // Add each letter as an element to array list
        for(int i = 0; i < r.length(); i++){
            romans.add(r.substring(i, i+1));
        }
        System.out.println(romans);
        // Call convert method
        System.out.println(r +" in decimal number is " + convert(romans));

    }

    // Method to translate each letter to number
    public static int romanDigit(String n){
        int d = 0;
        switch (n){
            case "I":
                d = 1;
                break;
            case "V":
                d = 5;
                break;
            case "X":
                d = 10;
                break;
            case "L":
                d = 50;
                break;
            case "C":
                d = 100;
                break;
            case "D":
                d = 500;
                break;
            case "M":
                d = 1000;
                break;
        }
        // return integer value of letter
        return d;
    }

    // Method for conversion algorithm
    public static int convert( ArrayList<String> r){
        int total = 0;
        int count = r.size();
        // While string is not empty
        while(count > 0){
            // If there is only one letter left for if the value of the first letter is at least the value of the second letter
            if(count == 1 || romanDigit(r.get(0)) >= romanDigit(r.get(1)) ){
                // Add numeric value of first letter to total
                total = romanDigit(r.get(0)) + total;
                // Remove the first element from array list
                r.remove(0);
                // Decrease count by 1
                count --;
            }
            else{
                // Else find the difference of the first two elements
                int diff = romanDigit(r.get(1)) - romanDigit(r.get(0));
                // Add difference to total
                total = total + diff;
                // Remove the first 2 elements
                r.remove(0);
                r.remove(0);
                // Decrease count by 2
                count = count -2;
            }
        }
        // Return numerical value
        return total;
    }
}

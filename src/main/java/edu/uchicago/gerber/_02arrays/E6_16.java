package edu.uchicago.gerber._02arrays;

import java.util.Arrays;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P6.16 Histogram
 * I wasn't able to figure out how to print vertically so I'm only printing horizontally :/
 * */

public class E6_16 {

    public static void main(String[] args) {
        // Get user input for 5 integers
        int [] data = new int [5];
        Scanner in = new Scanner(System.in);
        System.out.println("Enter five numbers: ");
        // Add values to array
        for(int i = 0; i<5; i++){
            data[i] = in.nextInt();
        }
        System.out.println(Arrays.toString(data));

        // Call method to find max value
        int m = findMax(data);
        // Printing graph horizontally
        for(int i = 0; i < data.length; i++){
            //
            int count = findCount(data[i], m);
            for(int j = 0; j< count; j++){
                System.out.print("*");
            }
            System.out.println(" ");
        }
    }

    public static int findMax (int [] a){
        int max = 0;
        for (int i = 1; i < a.length; i++){
            if (a[i] > max){
                max = a[i];
            }
        }
        return max;
    }

    // Finding star count proportional to 20 as max
    public static int findCount(int a, int max){
        int stars = 0;
        stars = (20 * a)/ max;
        return stars;
    }
}

package edu.uchicago.gerber._02arrays;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P7.5 CSV Reader
 *
 */

public class P7_5 {
    public static void main(String[] args) throws FileNotFoundException {

        try{
            // Array list for all the fields
            ArrayList<String> csvData = new ArrayList<>();
            // Array list for each row of data
            ArrayList<String> rows = new ArrayList<>();
            File input = new File("csv.txt");
            Scanner scan = new Scanner(input);
            // While there is next line
            while (scan.hasNextLine()) {
                String row = scan.nextLine();
                // Add each row as an element to rows
                rows.add(row);
                csvData=(fields(row));
            }

            System.out.println("Number of rows: " + numberOfRows(rows));
            System.out.println("Number of fields: " + numberOfFields(csvData));
            System.out.println("Fields: " + csvData);

            // Close scanner
            scan.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
    // Method to find fields
    public static ArrayList<String> fields(String row) {
        System.out.println(row);
        ArrayList<String> data = new ArrayList<>();
        // To distinguish between comma in quotes, we will toggle boolean as we loop through the string
        boolean inQuote = false;
        int begIn = 0;
        for (int curIn = 0; curIn < row.length(); curIn++) {
            // If the character at index is a quote
            if(row.charAt(curIn)=='\"'){
                // We set inQuote to !inQuote
                inQuote = !inQuote;
            }
            // It doesn't matter the actual value of the boolean
            // We just need to make sure that we did not encounter quotes prior to encountering a comma
            if (row.charAt(curIn)== ',' && !inQuote){
                // If that's the case we add the substring to the array list as an element
                data.add(row.substring(begIn,curIn).trim());
                // We update the beginning index to the current index + 1 to parse next element
                begIn = curIn + 1;
            }else if(curIn == row.length()-1 ){
                // For the last element, since there is no comma at the end, we just take the substring of
                // the beginning index to the end of the string
                data.add(row.substring(begIn,row.length()-1).trim());
            }
        }
        return data;
    }

    public static int numberOfRows(ArrayList<String> r){
        int numOfRows = r.size();
        return numOfRows;
    }

    public static int numberOfFields(ArrayList<String> f){
        int numOfFields = f.size();
        return numOfFields;
    }

}

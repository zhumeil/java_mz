package edu.uchicago.gerber._02arrays;
import java.util.Random;
/**
 *  * MPCS 51036
 *  * Meilin Zhu
 *  * P6.1 Array Initialization
 */

public class E6_1 {
    public static void main(String[] args) {
        // Initializing Random function
        Random r = new Random();
        // Initializing array of 9 integers
        int [] randomArray = new int[9];
        // Filling array with random integers
        for (int i = 0; i < randomArray.length; i++) {
            randomArray[i] = r.nextInt();
        }

        // Printing every element at every even index
        System.out.println("\nEvery element at an even index:");
        // We increment by 2 for every other number
        for(int i= 0; i<randomArray.length; i+=2){
            System.out.println(randomArray[i]);
        }

        // Printing every even element
        System.out.println("\nEvery even element: ");
        for(int element: randomArray){
            // Check if divisible by 2
            if(element%2 == 0){
                System.out.println(element);
            }
        }

        // All elements in reverse order
        System.out.println("\nArray in reverse order:");
        // We start index count at the last value and decrease by one to print backwards
        for(int i= randomArray.length -1; i>=0; i--){
            System.out.println(randomArray[i]);
        }

        // Only first and last elements
        System.out.println("\nOnly the first and last element: ");
        // The first element is at index 0 and the last index is the number of elements - 1
        int lastIndex = randomArray.length-1;
        System.out.println("First element: "+ randomArray[0]);
        System.out.println("Last element: "+ randomArray[lastIndex]);


    }
}

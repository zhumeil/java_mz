package edu.uchicago.gerber._02arrays;
import java.util.Arrays;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P6.9 Check for equality of elements
 * */

public class E6_9 {
    public static void main(String[] args) {

        // Get user input for the number of elements in first array
        Scanner in = new Scanner(System.in);
        System.out.println("Number of integers for the first array: ");
        int lengthA = in.nextInt();

        // Initialize first array with input of length for array A
        int[] arrayA = new int[lengthA];
        // Get input for elements
        System.out.println("Enter" + lengthA +  " integers for list A: ");
        for (int i = 0; i < lengthA; i++) {
            arrayA[i] = in.nextInt();
        }
        System.out.println(Arrays.toString(arrayA));

        // Get user input for the number of elements in second array
        System.out.println("Number of integers for the second array: ");
        int lengthB = in.nextInt();
        // Initialize first array with input of length for array B
        int[] arrayB = new int[lengthB];
        System.out.println("Enter" + lengthA +  " integers for list B: ");
        for (int i = 0; i < lengthB; i++) {
            arrayB[i] = in.nextInt();
        }
        System.out.println(Arrays.toString(arrayB));

        // Call equals method to determine equality, passing both arrays
        if (equals(arrayA, arrayB) == true) {
            System.out.println("The two arrays are equal");
        } else {
            System.out.println("The two arrays are NOT equal");
        }
    }

    public static boolean equals(int[] a, int[] b) {

        boolean isEqual = false;
        // First check lenght of array, if not the same, return false
        if(a.length != b.length){
            return isEqual;
        }

        // Sort both arrays so they are both in ascending order
        Arrays.sort(a);
        System.out.println(Arrays.toString(a));
        Arrays.sort(b);
        System.out.println(Arrays.toString(b));

        // Compare elements of arrays at the same index
        for (int i = 0; i < a.length; i++) {
            // If any value is not equal, return false
            if (a[i] != b[i]) {
                return isEqual;
            }
            // else set boolean to true
            else {
                isEqual = true;
            }
        }
        // Method returns boolean
        return isEqual;
    }
}

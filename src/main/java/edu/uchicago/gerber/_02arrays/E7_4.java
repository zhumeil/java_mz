package edu.uchicago.gerber._02arrays;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P7.4 Mary Had a Little Lamb
 * Be sure to set working directory to ../_02arrays in order to access lamb.txt
 */
public class E7_4 {
    public static void main(String[] args) throws FileNotFoundException {
        // Prompt user for name of input file
        // In this case the file is already in current directory and is named "lamb.txt"
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the input file name (example.txt): ");
        String inputFile = in.nextLine();
        // Prompt user for name of export file
        System.out.println("Please enter the output file name (example.txt): ");
        String outputFile = in.nextLine();

        // Initialize scanner and printer to read and write
        File input = new File(inputFile);
        Scanner scan = new Scanner(input);
        PrintWriter out = new PrintWriter(outputFile);
        // Initialize line count starting at 1
        int count = 1;
        // While file has next line, print line count and the line of text to outputFile
        // If outputFile does not exist, it will be created
        while (scan.hasNextLine()){
            String line = scan.nextLine();
            out.println("/* " + count + " */ " + line);
            count ++;
        }

        // Close scanner and printer
        scan.close();
        out.close();
    }
}

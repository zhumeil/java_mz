package edu.uchicago.gerber._02arrays;
import java.util.Arrays;
import java.util.Random;

/**
 * MPCS 51036
 * Meilin Zhu
 * P6.12 Random values and sort
 * */

public class E6_12 {

    public static void main(String[] args) {
        // Generate 20 random integers through getRandomNum method
        int [] ranNum = new int[20];
        for (int i = 0; i < ranNum.length; i++) {
            ranNum[i] = getRandomNum();
        }
        // Print array as is
        System.out.println("Sequence: " + Arrays.toString(ranNum));
        Arrays.sort(ranNum);
        // Print array sorted in ascending order
        System.out.println("Sorted sequence: " + Arrays.toString(ranNum));

    }
    // Generate random integers through Java Random Function with bound of 0 to 100
    public static int getRandomNum(){
        Random r = new Random();
        int ran = r.nextInt(99);
        return ran;
    }
}

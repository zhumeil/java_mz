package edu.uchicago.gerber._02arrays;
import java.util.Scanner;
/**
 * MPCS 51036
 * Meilin Zhu
 * P5.8 Leap year
 * */

public class P5_8 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Please enter a year: ");

        int year = in.nextInt();

        if (isLeapYear(year)){
            System.out.println("The year "+ year + " is a leap year");
        }
    }

    public static boolean isLeapYear(int year) {
        boolean isLeap = false;
        // If year is not divisible by 4, then it's automatically not a leap year
        if (year % 4 == 0){
            // If year is divisible by 100, then it's not a leap year
            if(year % 100 != 0){
                // If year passes all the previous if statements and is divisible by 400, then it's a leapyear
                if(year % 400 == 0){
                    isLeap = true;
                }
            }
        }

        return isLeap;
    }
}

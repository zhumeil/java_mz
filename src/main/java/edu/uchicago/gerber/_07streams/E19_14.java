package edu.uchicago.gerber._07streams;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**MPCS 51036
 * Meilin Zhu
 * E19.14 Parallel stream
 * Requires changing working directory to /_07streams/
 * */

public class E19_14 {

    public static boolean isPalindrome(String str){
        // Attempting to clean text
        String strClean = str.replaceAll("[^a-zA-Z]+","").toLowerCase().trim();
        StringBuilder stb = new StringBuilder(strClean);
        StringBuilder reverse = stb.reverse();
        return reverse.toString().equals(strClean);
    }
    public static void main(String [] args) throws IOException {
        ArrayList<String> wap = new ArrayList<>();
        // used p.txt to test isPalindrome
        // able to get a different word each time
        FileReader file = new FileReader("warAndPeace.txt");
        try (BufferedReader reader = new BufferedReader(file);) {
            while (reader.ready()) {
                wap.add(reader.readLine());
            }
        }
        // Parallel stream and filtering by length of word
        String palindrome = wap.stream().parallel().filter(s -> s.length() > 5).filter(s -> isPalindrome(s)).findAny().orElse("Nothing");
        System.out.println(palindrome + "\n");
    }
}

package edu.uchicago.gerber._07streams;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**MPCS 51036
 * Meilin Zhu
 * E19.5 Comma-separated Values
 * */
public class E19_5 {

    public static <T> String toString(Stream<T> stream, int n){

        Stream <String> stringStream = stream.limit(n).map(str -> str.toString());

        return stringStream.collect(Collectors.joining(", "));

    }

    public static void main (String [] args){

        System.out.println("Enter a stream of strings:\n");
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        String steamArray [] = s.split(" ");
        Stream <String> stringStream = Arrays.stream(steamArray);
        System.out.println(toString(stringStream, steamArray.length));



    }
}

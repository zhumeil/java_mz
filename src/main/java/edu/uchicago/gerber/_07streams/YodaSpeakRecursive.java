package edu.uchicago.gerber._07streams;

import java.util.Arrays;
import java.util.Scanner;

/**MPCS 51036
 * Meilin Zhu
 * Yoda Speak Recursive
 * */
public class YodaSpeakRecursive {

        public static String [] reverseRecurse(String [] words, int start, int end){
            String t = "";
            // Check if input is only one word, if so just return word
            if(start >= end)
                return words;
            else
                // We create a temp value to hold the first element
                t = words[start];
            // We swap the starting element with the ending element
            words[start] = words[end];
            words[end] = t;
            // Recursively work our way to the middle of array
            return reverseRecurse(words, start+1, end-1);
        }

        public static void main(String[] args){

            while(true){

                Scanner in = new Scanner(System.in);
                System.out.print("\nEnter a sentence: \n");
                String words = in.nextLine();
                if (words.toLowerCase() == "exit")
                    break;
                // We parse string into an array
                String wordsArray[] = words.split(" ");
                // we pass in the array, starting index, and ending index
                wordsArray = reverseRecurse(wordsArray, 0, wordsArray.length-1);
                for (int i = 0; i < wordsArray.length; i++) {
                    System.out.print(wordsArray[i] + " ");
                }
            }

        }
    }
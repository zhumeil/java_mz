package edu.uchicago.gerber._07streams;
import java.util.Scanner;

/**MPCS 51036
 * Meilin Zhu
 * E13.20 Paying for something with bills
 * I initially couldn't figure out how to do this recursively, so I just implemented an iterative solution
 * */
public class E13_20_iterative {

    static int amount;
    public static void findBillCombo(int amount) {

        if (amount<=0){}
        else {
            // initial amount
            int nA = amount;
            int mA = nA/100;
            // Number of possible $100
            for(int i = 0; i<= mA; i++){
                int nB = nA - i*100;
                int mB = nB/20;
                // Number of possible $20
                for(int j = 0; j<=mB ; j++){
                    // Find amount left after each addition of $20
                    int nC = nB - j*20;
                    int mC = nC/5;
                    // Number of possible $5
                    for(int k = 0; k<= mC; k++){
                        // Find amount lefter after each addition of $5 and that's the amount of $1
                        int nD = nC - k *5;
                        System.out.println("[$100]:" + i + " [$20]:"+ j + " [$5]:" + k + " [$1]: " + nD);

                    }
                }
            }

        }
    }
    public static void main(String [] args){

        while (true){
            Scanner in = new Scanner(System.in);
            System.out.println("\n Enter Price: ");
            int price = in.nextInt();
            amount = price;
            System.out.println("Ways to pay:");
            findBillCombo(price);
        }

    }
}

package edu.uchicago.gerber._07streams;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**MPCS 51036
 * Meilin Zhu
 * E13.3 Telephone words
 * */
public class P13_3 {

    static Character[][] charMap;
    static  List<String> stringList;

    private static List<String> printWords(int[] numbers, int len, int index, String s)
    {
        numLetterMap();
        if(len == index)
        {
            return new ArrayList<>(Collections.singleton(s));
        }

        List<String> stringList = new ArrayList<>();

        for(int i = 0;
            i < charMap[numbers[index]].length; i++)
        {
            // get string value of char
            String letter = String.valueOf(s.toCharArray());
            letter = letter.concat(charMap[numbers[index]][i].toString());
            // recursively find the letters
            stringList.addAll(printWords(numbers, len, index + 1, letter));
        }
        return stringList;
    }

    // Two dimensional array to map number to letters
    public static void numLetterMap() {
        charMap = new Character[10][5];
        charMap[0] = new Character[]{' '};
        charMap[1] = new Character[]{' '};
        charMap[2] = new Character[]{'A','B','C'};
        charMap[3] = new Character[]{'D','E','F'};
        charMap[4] = new Character[]{'G','H','I'};
        charMap[5] = new Character[]{'J','K','L'};
        charMap[6] = new Character[]{'M','N','O'};
        charMap[7] = new Character[]{'P','Q','R','S'};
        charMap[8] = new Character[]{'T','U','V'};
        charMap[9] = new Character[]{'W','X','Y','Z'};
    }

    public static void main(String[] args) throws IOException {
        FileReader file = new FileReader("words.txt");
        String word;
        ArrayList <String> wordList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(file);) {
            while (reader.ready()) {
                word = reader.readLine().toUpperCase();
                wordList.add(word);
            }
        }

        while (true) {
            System.out.print("Enter some numbers or 'exit': \n");
            Scanner in = new Scanner(System.in);
            String input = in.nextLine();
            if (input == "exit")
                break;
            int numbers[] = new int[input.length()];
            for (int i = 0; i < input.length(); i++) {
                numbers[i] = Integer.valueOf(input.substring(i, i + 1));
            }
            stringList = printWords(numbers, numbers.length, 0, "");
            System.out.println("All possible combinations:");
            for (String s : stringList) {
                System.out.println(s);
            }
            System.out.println("All real words:");
            for (String w : stringList) {
                if (filterWords(w, wordList))
                    System.out.println(w + "\n");
            }

        }

    }

    private static boolean filterWords(String input, ArrayList<String> list) {
        for(String w:list){
            return input.equals(w.toUpperCase());
        }
        return false;
    }

}





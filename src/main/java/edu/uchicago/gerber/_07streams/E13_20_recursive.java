package edu.uchicago.gerber._07streams;

import javax.sound.midi.SysexMessage;
import java.util.*;

/**MPCS 51036
 * Meilin Zhu
 * E13.20 Paying for something with bills
 * */

public class E13_20_recursive {

    private static List<Integer> bills;
    private static int count=0;

    public static void findBillCombo(int amount,int index,ArrayList<Integer> list)
    {

        // Once amount gets to 0
        if(amount==0)
        {
            Set<Integer> billSet = new HashSet<Integer>(list);
            count++;
            // Convert to Set to output count of each denomination
            System.out.println("Combination "+ count);
            for(int i: billSet){
                System.out.print("["+ i + "]" + " " + Collections.frequency(list,i) +"\n");
            }
            System.out.println(list.toString());
            return ;
        }
        // Base case
        if(amount < 0)
            return ;

        for(int i=index ; i < bills.size();i++)
        {
            int d = bills.get(i);
            if(amount >= d)
            {
                list.add(d);
                findBillCombo(amount - d ,i,list );
                list.remove(list.size()-1);

            }
        }
    }

    public static void main(String[] args) {
        // Initialize array list with the denominations
        bills = new ArrayList<>();
        bills.add(1);
        bills.add(5);
        bills.add(20);
        bills.add(100);

        while (true){
            Scanner in = new Scanner(System.in);
            System.out.println("\nEnter Price: ");
            int amount = in.nextInt();
            findBillCombo(amount,0,new ArrayList<Integer>());
            System.out.println("Ways to pay :" + count);
        }



    }
}

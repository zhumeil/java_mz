package edu.uchicago.gerber._07streams;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**MPCS 51036
 * Meilin Zhu
 * E19.16 GroupBy and collect
 * Requires changing working directory to /_07streams/
 * */
public class E19_16 {

    public static void main(String [] args){
        ArrayList<String> wrdList = new ArrayList<>();

        try{
            // used words.txt from MPCS50101
            Scanner in = new Scanner(new File("words.txt"));
            while(in.hasNext()){
                wrdList.add(in.next().toLowerCase());
            }

            Map<String, Double> wrdGrouping = wrdList
                    .stream()
                    .collect(Collectors.groupingBy(s -> s.substring(0,1), Collectors.averagingDouble(String::length)));

            for(Map.Entry<String,Double> set : wrdGrouping.entrySet()){
                System.out.println(set.getKey() + ":" + String.format("%,.2f", set.getValue()));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

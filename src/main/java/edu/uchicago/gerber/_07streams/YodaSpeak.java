package edu.uchicago.gerber._07streams;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**MPCS 51036
 * Meilin Zhu
 * Yoda Speak Iterative
 * */
public class YodaSpeak {
    public static void main(String[] args){

        while(true){

            Scanner in = new Scanner(System.in);
            System.out.print("Enter a sentence: \n");
            String words = in.nextLine();
            if (words.toLowerCase() == "exit")
                break;
            System.out.println(reverse(words));
        }


    }

    static String reverse(String words){
        // We convert string to stringBuilder in order to use the reverse method
        StringBuilder sb = new StringBuilder();
        // We create an array because we want to reverse by word not each letter
        String wordsArray[] = words.split(" ");
        // Reverse array
        Collections.reverse(Arrays.asList(wordsArray));
        // We rebuild string in reverse order
        for(String s: wordsArray)
            sb.append(s +" ");

        return sb.toString();
    }

}

package edu.uchicago.gerber._07streams;


import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**MPCS 51036
 * Meilin Zhu
 * E19.7 Transform Strings
 * */
public class E19_7 {

    public static void main (String [] args){

            // used the currency stream from 19_6
            Set<Currency> currencies = Currency.getAvailableCurrencies();
            Stream<Currency> currencyStream = currencies.stream();
            Stream<String> currencyName = currencyStream.map(c -> c.getDisplayName()).filter(c -> c.length() > 2);

            Stream<String> transformed = currencyName.map(s ->s.substring(0,1) + "..." + s.charAt(s.length()-1));
            List<String> transformedList = transformed.collect(Collectors.toList());

            for(String s: transformedList)
                    System.out.println(s);
    }

}

package edu.uchicago.gerber._07streams;
import java.util.Scanner;


/**MPCS 51036
 * Meilin Zhu
 * E13.4 Binary Recursively
 * */
public class E13_4 {

    public static void recurseBinary(int input){
        if(input > 0){
            // We recursively find all the value of input/2
            recurseBinary(input/2);
            // Then we find the remainders of those values/2
             System.out.print(input % 2);
        }
    }

    public static void main(String[] args){

        while (true){
            System.out.print("\n Enter number to convert to binary:\n");
            Scanner in = new Scanner(System.in);
            int number = in.nextInt();
            System.out.println("Binary of "+ number + ": " );
            recurseBinary(number);

        }

    }

}

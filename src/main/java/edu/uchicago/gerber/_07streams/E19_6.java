package edu.uchicago.gerber._07streams;

import java.util.Currency;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**MPCS 51036
 * Meilin Zhu
 * E19.5 Transform Currencies
 * */
public class E19_6 {

    public static void main (String [] args){
        // Set of Currencies
        Set<Currency> currencies = Currency.getAvailableCurrencies();
        Stream<Currency> currencyStream = currencies.stream();
        Stream<String> currencyName = currencyStream.map(c -> c.getDisplayName());
        List <String> currencyList = currencyName.sorted().collect(Collectors.toList());

        for(String c : currencyList)
            System.out.println(c);
    }
}

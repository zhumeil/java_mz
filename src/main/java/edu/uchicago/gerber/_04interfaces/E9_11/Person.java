package edu.uchicago.gerber._04interfaces.E9_11;

public class Person {


    public String name;
    public int yearOfBirth;

    /** Constructs a person with name and year of birth
        @param name the name of person
        @param yearOfBirth year the person was born
     */
    public Person(String name, int yearOfBirth){
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public String toString(){
        return "\nNAME: " +name + "\nYEAR OF BIRTH: " + yearOfBirth;
    }
}

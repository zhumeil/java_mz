package edu.uchicago.gerber._04interfaces.E9_11;

public class Student extends Person {

    private String major;

    // Constructs a student with name and year of birth and major
    public Student(String name, int yearOfBirth, String major) {
        super(name, yearOfBirth);
        this.major = major;
    }

    public String toString(){
        return super.toString() + "\nMAJOR: " + major;
    }
}

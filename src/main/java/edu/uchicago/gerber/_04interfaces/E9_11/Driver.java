package edu.uchicago.gerber._04interfaces.E9_11;

/**
 * MPCS 51036
 * Meilin Zhu
 * E9.11 Person
 */


public class Driver {
    public static void main(String[] args) {

        // Creating a Person of each type
        Person Ana = new Instructor("Ana", 1980, 120000);
        Person Bob = new Student("Bob", 1960, "Computer Science");
        Person Cody = new Person("Cody", 2000);

        System.out.println(Ana);
        System.out.println(Bob);
        System.out.println(Cody);


    }
}

package edu.uchicago.gerber._04interfaces.E9_11;

public class Instructor extends Person {

    private double salary;

    // Constructs an instructor with name and year of birth and salary
    public Instructor(String name, int yearOfBirth, double salary) {
        super(name, yearOfBirth);
        this.salary = salary;
    }

    public String toString(){
        return super.toString() + "\nSALARY: " + salary;
    }
}

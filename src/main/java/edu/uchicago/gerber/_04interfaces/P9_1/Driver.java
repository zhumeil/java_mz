package edu.uchicago.gerber._04interfaces.P9_1;

import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P9.1 Clock
 */

public class Driver {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        
        // create Clock of current time in central timezone
        Clock clock = new Clock();
        System.out.println("The current time is: " + clock.getTime());
        
        // create WorldClock that's 2 hours behind
        WorldClock worldClock1 = new WorldClock(-2);
        System.out.println("The current time in time zone 2 hours behind: " + worldClock1.getTime());

        // create WorldClock that's 2 hours ahead
        WorldClock worldClock2 = new WorldClock(2);
        System.out.println("The current time in time zone 2 hours ahead: " + worldClock2.getTime());

        // Get user input for timezone offset
        System.out.println("Enter time zone offset: ");
        int offSet = in.nextInt();
        WorldClock worldClock3 = new WorldClock(offSet);
        System.out.println("The current time in your time zone: " + worldClock3.getTime());

    }
}

package edu.uchicago.gerber._04interfaces.P9_1;


import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;


public class WorldClock extends Clock{
    private int offSet;
    private LocalTime now;

    // Construct world colock with offset
    public WorldClock(int offSet){
        this.offSet = offSet;
        this.now = LocalTime.now();
    }

    // We override getHours() and getMinutes() to add offset before returning both in getTime()
    @Override
    public String getHours(){
        ZoneOffset zoneOffset = ZoneOffset.ofHours(offSet);
        OffsetTime offSetHours = OffsetTime.now(zoneOffset);
        return offSetHours.toString().substring(0,2);
    }

    @Override
    public String getMinutes(){
        ZoneOffset zoneOffset = ZoneOffset.ofHours(offSet);
        OffsetTime offSetHours = OffsetTime.now(zoneOffset);
        return offSetHours.toString().substring(3,5);
    }

}

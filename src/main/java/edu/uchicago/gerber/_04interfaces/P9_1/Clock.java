package edu.uchicago.gerber._04interfaces.P9_1;

import java.time.LocalTime;

public class Clock {


    // LocalTime returns time in HH:MM:SS so to get just the hour and minutes we take the substring
    public String getHours(){
        return LocalTime.now().toString().substring(0,2);
    }

    public String getMinutes(){
        return LocalTime.now().toString().substring(3,5);
    }

    public String getTime(){
        return getHours() + ":" + getMinutes();
    }
}

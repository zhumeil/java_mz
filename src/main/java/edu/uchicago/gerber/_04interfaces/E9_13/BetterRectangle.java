package edu.uchicago.gerber._04interfaces.E9_13;

import java.awt.*;

public class BetterRectangle extends Rectangle {

    /**
     * BetterRectangle inherits x, y and width and height from Rectangle
     * */

    public BetterRectangle(int x, int y, int w, int h){
        setLocation(x, y); // location includes x and y coordinates
        setSize(w, h); // size includes width and height of rectangle
    }

    // calculate perimeter
    public double getPerimeter(){
        return getWidth() * 2 + getHeight() * 2;
    }

    // calculate area
    public double getArea(){
        return getWidth() * getHeight();
    }

}

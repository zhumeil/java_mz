package edu.uchicago.gerber._04interfaces.E9_13;

/**
 * MPCS 51036
 * Meilin Zhu
 * E9.13 Better Rectangle
 */
public class Driver {

    public static void main(String[] args) {

        BetterRectangle rectangle = new BetterRectangle(4,2, 5,7);
        System.out.println("PERIMETER: " + rectangle.getPerimeter());
        System.out.println("AREA: " + rectangle.getArea());

    }
}

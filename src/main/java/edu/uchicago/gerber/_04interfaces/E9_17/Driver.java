package edu.uchicago.gerber._04interfaces.E9_17;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * E9.17 Soda Can
 */
public class Driver {
    public static void main(String[] args) {

        Measurable [] sodaCan = new SodaCan[5];
        sodaCan[0] = new SodaCan(44,32);
        sodaCan[1] = new SodaCan(54,43);
        sodaCan[2] = new SodaCan(23,7);
        sodaCan[3] = new SodaCan(22,33);
        sodaCan[4] = new SodaCan(13,71);
        System.out.println("Average surface area: "+ averageArea(sodaCan));

    }

    // Method to find average surface area
    public static double averageArea(Measurable [] sodaCans) {
        double totalArea = 0;
        for (Measurable sodaCan : sodaCans) {
            totalArea = totalArea + sodaCan.getMeasurable();
        }
        return totalArea/sodaCans.length;
    }
}

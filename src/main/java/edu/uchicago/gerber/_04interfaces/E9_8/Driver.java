package edu.uchicago.gerber._04interfaces.E9_8;

import java.util.Scanner;

/**
* MPCS 51036
* Meilin Zhu
* E9.8 BankAccount
 */

public class Driver {

    public static void main(String[] args) {

        BankAccount[] accounts = new BankAccount[5];

        accounts[0] = new CheckingAccount();
        accounts[1] = new SavingsAccount();
        accounts[2] = new BasicAccount();
        accounts[3] = new BasicAccount();
        accounts[4] = new CheckingAccount();

        /** From 9.1 How To  */
        Scanner in = new Scanner(System.in);
        boolean done = false;
        while (!done) {
            System.out.print("D)eposit  W)ithdraw  M)onth end  Q)uit: ");
            String input = in.next();
            if (input.equals("D") || input.equals("W")) { // Deposit or withdrawal

                System.out.print("Enter account number and amount: ");
                int num = in.nextInt();

                double amount = in.nextDouble();
                if (input.equals("D")) {
                    accounts[num].deposit(amount);
                } else {
                    accounts[num].withdraw(amount);
                }

                System.out.println("Balance: " + accounts[num].getBalance());
            } else if (input.equals("M")) { // Month end processing
                for (int n = 0; n < accounts.length; n++) {
                    accounts[n].monthEnd();
                    System.out.println(n + " " + accounts[n].getBalance());
                }
            } else if (input.equals("Q")) {
                done = true;
            }

        }
    }
}

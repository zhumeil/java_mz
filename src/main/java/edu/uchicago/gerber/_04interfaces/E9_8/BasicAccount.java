package edu.uchicago.gerber._04interfaces.E9_8;

public class BasicAccount extends BankAccount{

    /** Constructs a basic account with a zero balance.   */
    public BasicAccount(){}

    // These methods override superclass methods
    public void withdraw(double amount) {
        double balance = getBalance();
        if (amount > balance) {
            System.out.println("NOT ENOUGH BALANCE");
        }else
            super.withdraw(amount);
    }

}

package edu.uchicago.gerber._04interfaces.P9_6;

public class Monthly extends Appointments{


    public Monthly(int month, int day, int year, String description) {
        super(month, day, year, description);
    }

    // For daily appointments, the year and month must be equal or greater than the current
    // The day must be the exact same
    // Assuming we are not looking at dates prior to appointment date
    @Override
    public boolean occursOn(int m, int d, int y) {
        if(y>=getYear() && m >= getMonth() && d==getDay())
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return "[MONTHLY] " + super.toString();
    }
}

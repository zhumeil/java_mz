package edu.uchicago.gerber._04interfaces.P9_6;

import java.util.Date;

public class Appointments {

    private String description;
    private int month;
    private int day;
    private int year;

    // Construct appointments with mm dd yyyy and description
    public Appointments(int month, int day, int year, String description ){
        this.description = description;
        this.month = month;
        this.day = day;
        this.year = year;
    }

    // boolean that matches the month, day and year
    public boolean occursOn( int m, int d, int y){
        if(month == m && day == d && year == y )
            return true;
        else return false;
    }

    // print all appointment details
    public String toString(){
        return month + "/" + day + "/" + year +  ": " + description;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

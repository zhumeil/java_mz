package edu.uchicago.gerber._04interfaces.P9_6;

import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P9.6 Appointments
 */
public class Driver {
    public static void main(String[] args) {

        // Creating appointments
        Appointments [] appointments = new Appointments[8];

        appointments[0] = new Monthly(10, 1, 2020, "Pay rent");
        appointments[1] = new Daily(10, 25, 2018, "Walk dog");
        appointments[2] = new Monthly(7, 20, 2022, "Make student loan payment");
        appointments[3] = new Onetime(11, 2, 2021, "Make reservation");
        appointments[4] = new Daily(1, 1, 2019, "Meditate" );
        appointments[5] = new Onetime(11, 25, 2022, "Thanksgiving");
        appointments[6] = new Monthly(3, 11, 2018, "Call parents");
        appointments[7] = new Daily(5, 11, 2020, "Exercise");

        // Ask user to input date
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a date (mm dd yyyy): ");
        int mm = in.nextInt();
        int dd = in.nextInt();
        int yyyy = in.nextInt();

        // loop through array and display all matching appointments
        for(int i = 0; i<appointments.length; i++){
            if(appointments[i].occursOn(mm, dd, yyyy)){
                System.out.println(appointments[i].toString());
            }
        }
    }

}

package edu.uchicago.gerber._04interfaces.P9_6;

public class Onetime  extends Appointments {

    public Onetime(int month, int day, int year, String description) {
        super(month, day, year, description);
    }

    // One time occursOn is the same as the superclass
    @Override
    public String toString() {
        return "[ONE TIME] " + super.toString();
    }
}

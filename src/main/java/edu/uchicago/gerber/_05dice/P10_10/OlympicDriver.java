package edu.uchicago.gerber._05dice.P10_10;

import javax.swing.*;

/**
 * MPCS 51036
 * Meilin Zhu
 * E10.10 Olympic Rings
 * Write a program that displays the Olympic rings. Color the rings in the Olympic colors.
 * Provide a method drawRing that draws a ring of a given position and color*/
public class OlympicDriver {

    public static void main(String[] arg){

        JFrame jFrame = new JFrame();
        jFrame.setSize(600, 400);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        JComponent jComponent = new OlympicComponent();
        jFrame.add(jComponent);
        jFrame.setVisible(true);

    }

}

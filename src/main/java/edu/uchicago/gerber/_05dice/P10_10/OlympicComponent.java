package edu.uchicago.gerber._05dice.P10_10;

import javax.swing.*;
import java.awt.*;

public class OlympicComponent extends JComponent {

    public void paintComponent(Graphics g) {

        //Grpahics2D to modify stroke width to get thicker lines
        Graphics2D g2 = (Graphics2D) g;

        g2.setStroke(new BasicStroke(5));

        drawRing(g,100, 100, 100, 100, Color.BLUE);
        drawRing(g,205, 100, 100, 100, Color.BLACK);
        drawRing(g,310, 100, 100, 100, Color.RED);
        drawRing(g,150, 150, 100, 100, Color.YELLOW);
        drawRing(g,255, 150, 100, 100, Color.GREEN);

    }

    public void drawRing(Graphics g, int x, int y, int w, int h, Color c){
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(5));
        g.setColor(c);
        g.drawOval(x, y, w, h );
    }
}



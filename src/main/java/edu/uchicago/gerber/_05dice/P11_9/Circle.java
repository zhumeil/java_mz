package edu.uchicago.gerber._05dice.P11_9;

import java.awt.*;

public class Circle {

    private Point center, outer;
    public Circle(Point center, Point outer){
        this.center = center;
        this.outer = outer;
    }

    public void paintComponent(Graphics g) {
        if (center != null && outer != null) {
            int r = (int) Math.round(center.distance(outer));
            g.drawOval(center.x - r, outer.y - r, 2 * r, 2 * r);
        }

    }
}

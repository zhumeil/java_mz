package edu.uchicago.gerber._05dice.P11_9;


import javax.swing.*;
import java.awt.*;

/**
 * MPCS 51036
 * Meilin Zhu
 * E11.9*/

public class CircleDriver {
    public static void main(String[] arg){

        JFrame jFrame = new JFrame();

        JComponent jComponent = new CircleMaker();
        jFrame.add(jComponent);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.pack();
        jFrame.setVisible(true);
        jFrame.setSize(new Dimension(800, 600));

    }

}

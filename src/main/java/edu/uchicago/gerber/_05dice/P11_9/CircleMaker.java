package edu.uchicago.gerber._05dice.P11_9;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CircleMaker extends JComponent {


    private Point center, outer;

    public CircleMaker() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (center == null || outer != null) {
                    center = e.getPoint();
                    outer = null;
                } else
                    outer = e.getPoint();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                Circle circle = new Circle(center, outer);
                circle.paintComponent(getGraphics());
            }
        });

    }
}


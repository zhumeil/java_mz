package edu.uchicago.gerber._05dice.P10_19;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BillFrame extends JFrame {

    private JButton b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, enter;
    private JTextField total, output, customItem, customItemPrice;;
    private JLabel itemsOrdered, other, billTotal, tip, itemAdded;
    private static final int FRAME_WIDTH = 800;
    private static final int FRAME_HEIGHT = 600;
    private static final double TAX = 1.1025;
    private double bt;

    public BillFrame(){
        createComponents();
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
    }

    ActionListener listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            String item = e.getActionCommand();
            Double itemPrice ;
            if (item == "Enter"){
                item  = customItem.getText();
                itemPrice = Double.parseDouble(customItemPrice.getText());
            }
            else{

                MenuItems menuItems = new MenuItems(item);
                itemPrice = menuItems.getItemPrice();
            }

            bt = bt + itemPrice;
            output.setText(item);
            total.setText("$" + String.format("%.2f",bt));
            tip.setText("Suggested Tip: $" + String.format("%.2f", bt * .18));


        }
    };

    public void createComponents(){

        Font f1 = new Font("SansSerif", Font.BOLD, 20);

        b1 = new JButton("Big Mac");
        b2 = new JButton("McDouble");
        b3 = new JButton("Quarter Pounder");
        b4 = new JButton("Cheese Burger");

        b5 = new JButton("Fries");
        b6 = new JButton("McNuggets");
        b7 = new JButton("Spicy Chicken Sandwich");
        b8 = new JButton("McRibs");


        b9 = new JButton("McFlurry");
        b10 = new JButton("Shake");

        enter = new JButton("Enter");

        itemsOrdered = new JLabel("Choose Items:\n ");
        itemsOrdered.setFont(f1);

        other = new JLabel("Add Other Items");
        billTotal = new JLabel("Total + 10.25% Tax:");
        tip = new JLabel("Suggested Tip: " );
        itemAdded = new JLabel("Item Added");
        customItem = new JTextField(8);
        customItemPrice= new JTextField(8);
        output = new JTextField(12);
        total = new JTextField(8);

        JPanel p = new JPanel();
        JPanel pMain = new JPanel();
        JPanel pSummary = new JPanel();
        pSummary.setPreferredSize(new Dimension(600, 80));
        p.setLayout(new BorderLayout());
        p.add(itemsOrdered, BorderLayout.NORTH);

        pMain.setLayout(new GridLayout(3,4));
        pMain.add(b1);
        pMain.add(b2);
        pMain.add(b3);
        pMain.add(b4);
        pMain.add(b5);
        pMain.add(b6);
        pMain.add(b7);
        pMain.add(b8);
        pMain.add(b9);
        pMain.add(b10);

        pMain.add(other);
        pMain.add(customItem);
        pMain.add(customItemPrice);
        pMain.add(enter);

        pSummary.add(itemAdded);
        pSummary.add(output);
        pSummary.add(billTotal);
        pSummary.add(total);
        pSummary.add(tip);
        p.add(pMain, BorderLayout.CENTER);
        p.add(pSummary, BorderLayout.SOUTH);
        add(p);

        b1.addActionListener(listener);
        b2.addActionListener(listener);
        b3.addActionListener(listener);
        b4.addActionListener(listener);
        b5.addActionListener(listener);
        b6.addActionListener(listener);
        b7.addActionListener(listener);
        b8.addActionListener(listener);
        b9.addActionListener(listener);
        b10.addActionListener(listener);
        enter.addActionListener(listener);

    }

}

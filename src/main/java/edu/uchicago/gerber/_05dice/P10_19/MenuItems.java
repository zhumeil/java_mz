package edu.uchicago.gerber._05dice.P10_19;

public class MenuItems {

    private String itemName;
    private double itemPrice;

    public MenuItems(String itemName){
        this.itemName = itemName;
    }

    public double getItemPrice(){
        switch (itemName) {
            case "Big Mac": itemPrice = 4.79; break;
            case "McDouble": itemPrice = 2.39;  break;
            case "Quarter Pounder": itemPrice = 6.23; break;
            case "Cheese Burger": itemPrice = 2.03; break;
            case "Fries": itemPrice = 2.19; break;
            case "McNuggest": itemPrice = 4.55; break;
            case "Spicy Chicken Sandwich": itemPrice = 5.03; break;
            case "McRibs": itemPrice = 4.45; break;
            case "McFlurry": itemPrice = 4.19; break;
            case "Shake": itemPrice = 3.35; break;
        }
        return itemPrice;
    }
}

package edu.uchicago.gerber._05dice.P10_19;

import javax.swing.*;

/**
 * MPCS 51036
 *  Meilin Zhu
 *  P10.19
 *  Write a graphical application that produces a restaurant bill.
 * Provide buttons for ten popular dishes or drink items. (You decide on the items and their prices.)
 * Provide text fields for entering less popular items and prices.
 * In a text area, show the bill, including tax and a suggested tip.
 * */
public class BillDriver {
    public static void main(String[] arg){

        JFrame jFrame = new BillFrame();
        jFrame.setTitle("Bill Calculator");
        jFrame.setVisible(true);

    }

}

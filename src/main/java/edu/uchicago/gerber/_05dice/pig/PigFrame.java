package edu.uchicago.gerber._05dice.pig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class PigFrame extends JFrame {

    private JButton roll, hold;
    private JLabel turn, score1, score2, title, number;

    private int yourScore, robotScore;

    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 600;
    private final String PLAYER = "YOUR TURN";
    private final String ROBOT = "BOT'S TURN";


    public PigFrame() {
        createComponent();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    ActionListener listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            String btn = e.getActionCommand();
            if (btn == "ROLL") {
                // get number from dice
                int n = getNumber();
                // display number
                number.setText("You rolled: " + n);
                // if number is one, switch turn to robot
                if (n == 1) {
                    // reset your score to zero
                    yourScore = 0;
                    score1.setText("YOUR SCORE: " + yourScore);
                    turn.setText("YOU ROLLED 1! " + ROBOT);
                    // auto roll for robot
                    autoRoll();
                } else
                    // if not 1, add to your score
                    yourScore += n;
                if (yourScore >= 100) {
                    number.setText("CONGRATS YOU WON :D");
                } else
                    score1.setText("YOUR SCORE: " + yourScore);
            } else if (btn == "HOLD") {
                autoRoll();
            }

        }
    };

    public void createComponent() {
        Font fTitle = new Font("SansSerif", Font.BOLD, 20);
        Font fBold = new Font("SansSerif", Font.BOLD, 16);
        Font fBold2 = new Font("SansSerif", Font.BOLD, 14);

        roll = new JButton("ROLL");
        roll.setPreferredSize(new Dimension(100, 40));
        hold = new JButton("HOLD");
        hold.setPreferredSize(new Dimension(100, 40));

        roll.addActionListener(listener);
        hold.addActionListener(listener);


        title = new JLabel(" WELCOME TO PIG GAME ");
        title.setFont(fTitle);

        score1 = new JLabel("YOUR SCORE: 0");
        score1.setFont(fBold);
        score1.setForeground(Color.magenta);

        score2 = new JLabel("BOT'S SCORE: 0");
        score2.setFont(fBold);
        score2.setForeground(Color.blue);

        number = new JLabel();
        number.setFont(fBold);
        number.setForeground(Color.ORANGE);
        number.setHorizontalAlignment(JLabel.CENTER);
        turn = new JLabel("YOUR TURN");
        turn.setFont(fBold2);
        turn.setHorizontalAlignment(JLabel.CENTER);

        JPanel p = new JPanel();
        JPanel pMid = new JPanel();
        JPanel pBtn = new JPanel();
        JPanel pBottom = new JPanel();

        p.add(title, BorderLayout.NORTH);

        pMid.setLayout(new GridLayout(3, 1));
        pMid.setPreferredSize(new Dimension(400, 350));

        pBottom.add(score1);
        pBottom.add(score2);

        pBtn.add(roll);
        pBtn.add(hold);

        pMid.add(turn);
        pMid.add(number);

        p.add(pMid, BorderLayout.CENTER);
        p.add(pBtn, BorderLayout.CENTER);
        p.add(pBottom, BorderLayout.SOUTH);
        add(p);


    }

    // randomly generate dice number 1 to 6
    public int getNumber() {
        Random r = new Random();
        return r.nextInt(6 + 1 - 1) + 1;
    }

    // randomly generate number of turns between 1 and 20
    public int getTurns() {
        Random r = new Random();
        return r.nextInt(20 + 1 - 1) + 1;
    }

    // auto roll for robot
    public void autoRoll() {
        turn.setText(ROBOT);
        int turns = getTurns();
        int count = 0;
            while (count <= turns) {
                int n = getNumber();
                number.setText("Bot Rolled: " + n);
                if (n == 1) {
                    robotScore = 0;
                    score2.setText("BOT SCORE: " + robotScore);
                    turn.setText("BOT ROLLED 1! " + PLAYER);
                    break;
                } else {
                    // append robot score and display
                    robotScore += n;
                    if (robotScore >= 100) {
                        number.setText("SORRY YOU LOST :( ");
                    }
                    score2.setText("BOT SCORE: " + robotScore);
                    turn.setText("BOT TURN OVER!  " + PLAYER);
                    count++;
                }
            }

    }
}

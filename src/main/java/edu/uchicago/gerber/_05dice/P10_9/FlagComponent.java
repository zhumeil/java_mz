package edu.uchicago.gerber._05dice.P10_9;

import javax.swing.*;
import java.awt.*;

public class FlagComponent extends JComponent {

    public void paintComponent(Graphics g ){

        // Italian Flag
        g.setColor(Color.red);
        g.fillRect(100, 100, 30, 60);
        g.setColor(Color.green);
        g.fillRect(160, 100, 30, 60);
        g.setColor(Color.white);
        g.drawLine(130, 100, 160, 100);
        g.drawLine(130, 160, 160, 160);

        // Germany Flag
        g.setColor(Color.black);
        g.fillRect(100, 200, 90, 20);
        g.setColor(Color.red);
        g.fillRect(100, 220, 90, 20);
        g.setColor(Color.yellow);
        g.fillRect(100, 240, 90, 20);

        // Hungary Flag
        g.setColor(Color.red);
        g.fillRect(100, 300, 90, 20);
        g.setColor(Color.white);
        g.fillRect(100, 320, 90, 20);
        g.setColor(Color.green);
        g.fillRect(100, 340, 90, 20);

    }

}

package edu.uchicago.gerber._05dice.P10_9;

import javax.swing.*;
import java.util.Locale;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * E10.9 Italian Flag
 * Write a program that displays the German and Hungarian flags.
 * */
public class FlagDriver {

    public static void main(String[] arg){

        JFrame jFrame = new JFrame();

        jFrame.setSize(400, 600);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JComponent jComponent = new FlagComponent();
        jFrame.add(jComponent);
        jFrame.setVisible(true);

    }
}

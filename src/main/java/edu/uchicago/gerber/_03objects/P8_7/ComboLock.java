package edu.uchicago.gerber._03objects.P8_7;

public class ComboLock {
    int aSecret1, aSceret2, aSecret3;
    int aTry1, aTry2, aTry3;
    int count = 0;

    // Initializing object with the combination
    public ComboLock(int secret1, int secret2, int secret3) {
        aSecret1 = secret1;
        aSceret2 = secret2;
        aSecret3 = secret3;
    }

    // resets all values to 0
    public void reset() {
        aSecret1 = 0;
        aSceret2 = 0;
        aSecret3 = 0;
        aTry1 = 0;
        aTry2 = 0;
        aTry3 = 0;
    }
    // we only turn left once during the second try
    public void turnLeft(int ticks) {
        aTry2 = ticks;
    }
    // Since we turn right twice, we need to check which try it is
    // So after the first try is complete we increment the count
    public void turnRight(int ticks) {
        if (count == 0) {
            aTry1 = ticks;
            count++;
        }
        else
            aTry3 = ticks;
    }
    // all three numbers must match in order for method to return true
    public boolean open(int try1, int try2, int try3) {
        if(try1 == aSecret1 && try2 == aSceret2 && try3 == aSecret3){
            return true;
        }else
            return false;
    }

    /**Getters & Setters*/
    public int getaSecret1() {
        return aSecret1;
    }

    public void setaSecret1(int aSecret1) {
        this.aSecret1 = aSecret1;
    }

    public int getaSceret2() {
        return aSceret2;
    }

    public void setaSceret2(int aSceret2) {
        this.aSceret2 = aSceret2;
    }

    public int getaSecret3() {
        return aSecret3;
    }

    public void setaSecret3(int aSecret3) {
        this.aSecret3 = aSecret3;
    }
}

package edu.uchicago.gerber._03objects.P8_7;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P8.7 Combo Lock
 * */
public class Driver {


    public static void main(String[] args) {

        // Ask user to set combination by entering 3 integers
        Scanner in = new Scanner(System.in);
        System.out.println("To set your combination, please enter 3 numbers between 0 to 39: \n");

        // User array to check if each number is between 0 and 39, if not the number is not value and user needs to restart
        int [] combo = new int [3];
        for(int i = 0; i < combo.length; i++){
            int secret = in.nextInt();
            if(secret >= 0 && secret <=39){
                combo[i] = secret;
            }else {
                System.out.println("Invalid number entered :(");
                i-- ;
            }
        }
        System.out.println("Your combination has been set!");

        // Create object with the numbers entered
        ComboLock cl = new ComboLock(combo[0], combo[1], combo[2]);
        // While the lock is not open, prompt user to enter the 3 numbers
        boolean notOpen = true;
        while (notOpen) {
            System.out.println("To Open your lock first turn the dial to the right, then left, then right again.");
            System.out.println("Enter the number of ticks for number 1: ");
            int t1 = in.nextInt();
            cl.turnRight(t1);
            System.out.println("Enter the number of ticks for number 2: ");
            int t2 = in.nextInt();
            cl.turnLeft(t1);
            System.out.println("Enter the number of ticks for number 3: ");
            int t3 = in.nextInt();
            cl.turnRight(t3);
            // if all three number match  then provide success message
            if (cl.open(t1, t2, t3) == true) {
                notOpen = false;
                System.out.println("Your lock is now open, press [r] to reset your lock");
                String reset = in.next();
                if (reset == "r") cl.reset();
                System.out.println("Your lock has been reset to 0, 0, 0");
            } else
                // if combination is wrong, user can try again
                System.out.println("Wrong combination, please try again.\n");
        }

    }
}

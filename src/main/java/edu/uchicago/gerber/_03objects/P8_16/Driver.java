package edu.uchicago.gerber._03objects.P8_16;

import java.util.Scanner;
/**
 * MPCS 51036
 * Meilin Zhu
 * P8.16 Mailbox & Message
 * */
public class Driver {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int select = 0;

        Message [] m = new Message [3];
        Mailbox mb = new Mailbox(select);
        boolean br = true;
        // Initialize message object with sender and recipient
        m[0] = new Message("Sally", "Annie");
        m[1] = new Message("Sally", "Bob");
        m[2] = new Message("Sally", "Curtis");

        // Populate message for each recipient
        m[0].append("Dear Annie,\n");
        m[1].append("Dear Bob,\n");
        m[2].append("Dear Curtis\n");
        // Add message to mailbox
        mb.addMessage(m[0]);
        mb.addMessage(m[1]);
        mb.addMessage(m[2]);

        while (br){
            // Provide user with choices for commands
            System.out.println("\nMAILBOX \n[1]Compose \n[2]Read \n[3]Delete");
            int action = in.nextInt();
            switch (action){
            case 1:
                System.out.println("Which email would you like to compose? ");
                // Output all items in mailbox
                mb.getInbox(m);
                // index will be the selection - 1
                select = in.nextInt()-1;
                in.nextLine();
                // prompt user to add to message
                System.out.println("Add to message: ");
                String add = in.nextLine();
                // append message
                m[select].append(add + "\n");
                // print the complete message
                System.out.println("Message added\n" + m[select].toString());
                break;
            case 2:
                // Similar flow for reading messages
                System.out.println("Which message would you like to read? ");
                mb.getInbox(m);
                select = in.nextInt()-1;
                in.nextLine();
                // We retrieve the message from the mailbox object
                Message message = mb.getMessage(select);
                // print message through toString method
                System.out.println(message.toString());
                break;
             case 3:
                 // Prompt user to delete
                System.out.println("Which message would you like to delete? ");
                mb.getInbox(m);
                select = in.nextInt()-1;
                in.nextLine();
                mb.removeMessage(select);
                System.out.println("Message deleted");
                // We end the loop after message deletion
                br = false;
                break;
            }

        }

    }

}

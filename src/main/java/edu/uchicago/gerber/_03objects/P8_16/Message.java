package edu.uchicago.gerber._03objects.P8_16;

public class Message {

    private String mSender;
    private String mRecipient;
    private String mMessage ="";

    // Each message has a sender and recipient
    public Message(String sender, String recipient){
        mSender = sender;
        mRecipient = recipient;
    }
    // Append message from user input to existing message
    public void append(String text){
        mMessage = mMessage + text;
    }
    // Print complete message with sender and recipient info
    public String toString (){
        String message = "FROM: " + mSender +"\n"+  "TO: " + mRecipient + "\nMESSAGE: " + mMessage;
        return message;
    }

    /**Getters & Setters*/
    public String getmSender() {
        return mSender;
    }


    public void setmSender(String mSender) {
        this.mSender = mSender;
    }

    public String getmRecipient() {
        return mRecipient;
    }

    public void setmRecipient(String mRecipient) {
        this.mRecipient = mRecipient;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }
}

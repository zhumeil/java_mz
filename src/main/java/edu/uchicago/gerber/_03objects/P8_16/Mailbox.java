package edu.uchicago.gerber._03objects.P8_16;

public class Mailbox{

    private int mCount = 0;
    Message [] message = new Message[3];

    public Mailbox(int count){
        mCount = count;
    }
    // Add message at index
    public void addMessage(Message m){
        message[mCount] = m;
        mCount++;
    }

    // Method to display all messages in mailbox
    public String getInbox(Message m []){
        String inbox ="";
        for(int i = 0; i < m.length; i++){
            int count = i+1;
            System.out.println(inbox = "["+ count + "]" + " TO: " + m[i].getmRecipient());
        }
        return inbox;
    }

    public Message getMessage(int i){
        return message[i];
    }

    public void removeMessage(int i){
        message[i] = null;
    }
}

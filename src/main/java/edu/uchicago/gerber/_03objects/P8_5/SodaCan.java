package edu.uchicago.gerber._03objects.P8_5;

public class SodaCan {

    private double aHeight;
    private double aRadius;

    // Constructor initializing height and radius
    public SodaCan (double height, double radius){
        aHeight = height;
        aRadius = radius;
    }

    // Finding volume via volume formula
    public double getVolume(){
        double volume = Math.PI * Math.pow(aRadius, 2) * aHeight;
        return volume;
    }
    // Finding surface area via surface area formula
    public double getSurfaceArea(){
        double sa = 2 * Math.PI * aRadius * aHeight + 2 * Math.PI * Math.pow(aRadius, 2);
        return sa;
    }

    /**Getters & Setters*/
    public double getaHeight() {
        return aHeight;
    }

    public void setaHeight(double aHeight) {
        this.aHeight = aHeight;
    }

    public double getaRadius() {
        return aRadius;
    }

    public void setaRadius(double aRadius) {
        this.aRadius = aRadius;
    }


}

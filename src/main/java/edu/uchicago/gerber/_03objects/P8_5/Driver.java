package edu.uchicago.gerber._03objects.P8_5;


import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P8.5 Soda Can
 * */

public class Driver {
    public static void main(String[] args) {

        // Get height and radius of soda can
        Scanner in = new Scanner(System.in);
        System.out.println("What is the height of the soda can? ");
        double h = in.nextDouble();

        System.out.println("What is the radius of the soda can? ");
        double r = in.nextDouble();
        // create new soda can object
        SodaCan sc = new SodaCan(h, r);
        // call SodaCan class to get volume and surface area
        System.out.println("Volume: " + sc.getVolume());
        System.out.println("Volume: " + sc.getSurfaceArea());
    }
}

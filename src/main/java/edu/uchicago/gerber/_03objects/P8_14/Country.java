package edu.uchicago.gerber._03objects.P8_14;

public class Country {

    String cName;
    long cPopulation;
    double cArea;

    // Constructor to initialize name, population and area
    public Country(String name, long population, double area){
        cName = name;
        cPopulation = population;
        cArea = area;
    }
    // Method to find country with the max population
    public static String maxPop(Country [] countries){
        long maxPop = countries[0].getcPopulation();
        String maxPopCountry = countries[0].getcName();
        // Loop through array and compare population value to current max value
        for(int i = 0 ; i < countries.length; i++){
            if( countries[i].getcPopulation() > maxPop) {
                maxPop = countries[i].getcPopulation();
                maxPopCountry = countries[i].getcName();
            }
        }// return the value and the corresponding country name
        return maxPopCountry + " with " + maxPop + " people ";
    }

    // Method to find country with max area
    public static String maxArea(Country [] countries){
        double maxArea = countries[0].getcArea();
        String maxAreaCountry = countries[0].getcName();
        for(int i = 0 ; i < countries.length; i++){
            if(countries[i].getcArea() > maxArea){
                maxArea = countries[i].getcArea();
                maxAreaCountry = countries[i].getcName();
            }
        }// return the value and the corresponding country name
        return maxAreaCountry + " with " + maxArea + " km^2";
    }
    // Method to find country with max population density which equals population / area
    public static String maxPopDen(Country [] countries){
        double maxPopDen = countries[0].getcPopulation()/countries[0].getcArea();
        String maxPopDenCountry = countries[0].getcName();
        for(int i = 0 ; i < countries.length; i++){
            double cPopDen =  countries[i].getcPopulation()/countries[i].getcArea();
            if(cPopDen> maxPopDen){
                maxPopDen = cPopDen;
                maxPopDenCountry = countries[i].getcName();
            }
        }// return the value and the corresponding country name
        return maxPopDenCountry + " with " + maxPopDen + " P/km^2";
    }

    /**Getters & Setters*/
    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public long getcPopulation() {
        return cPopulation;
    }

    public void setcPopulation(long cPopulation) {
        this.cPopulation = cPopulation;
    }

    public double getcArea() {
        return cArea;
    }

    public void setcArea(int cArea) {
        this.cArea = cArea;
    }
}

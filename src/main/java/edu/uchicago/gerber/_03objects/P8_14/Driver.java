package edu.uchicago.gerber._03objects.P8_14;


/**
 * MPCS 51036
 * Meilin Zhu
 * P8.14 Country
 * */
public class Driver {

    public static void main(String[] args) {

        // Create array of country objects
        Country [] countries = new Country[5];
        // Instantiate objects
        // Data found at https://www.worldometers.info/world-population/population-by-country/
        countries[0] = new Country("China", 1439323776, 9388211);
        countries[1] = new Country("India", 1380004385, 2973190);
        countries[2] = new Country("US", 331002651, 9147420);
        countries[3] = new Country("France", 65273511, 547557);
        countries[4] = new Country("Russia", 145934462, 16376870);

        // Call methods in Country class to find max population, area, and population density
        System.out.println("Largest Population: " + Country.maxPop(countries) );
        System.out.println("Largest Area: " + Country.maxArea(countries));
        System.out.println("Largest Population Density: " + Country.maxPopDen(countries));
    }

}

package edu.uchicago.gerber._03objects.P8_1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P8.1 Microwave
 * */
public class Driver {
    public static void main(String[] args) {
        // create microwave object with 0 sec and power of 1 as default
        Microwave microwave = new Microwave(0, 1);
        // print commands for user
        System.out.println("MICROWAVE CONTROL PANEL:");
        System.out.println("[1] +30 Sec");
        System.out.println("[2] Switch Power Level");
        System.out.println("[3] Reset");
        System.out.println("[4] Start");

        // Switch statement for commands
        Scanner in = new Scanner(System.in);
        boolean input = true;
        while (input) {
            int instruction = in.nextInt();
            {
                switch (instruction) {
                    case 1:
                        // append 30 seconds each time [1] is chosen
                        microwave.addTime();
                        System.out.println("TIME: + 30");
                        break;
                    case 2:
                        // Toggle between level 1 and 2
                        microwave.switchPower();
                        System.out.println("POWER: " + microwave.getmPower());
                        break;
                    case 3:
                        // reset things to default
                        System.out.println("TIME: 0 sec \n POWER: 1");
                        microwave.restart();
                        break;
                    case 4:
                        // start
                        System.out.println(microwave.start());
                        input = false;
                        break;
                }
            }
        }

    }
}

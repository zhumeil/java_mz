package edu.uchicago.gerber._03objects.P8_1;

public class Microwave {

    private int mTime;
    private int mPower;

    // Constructor initializing time and power
    public Microwave(int time, int power){
        mPower = power;
        mTime = time;
    }
    // Adding 30 to time, since it's all in seconds, there's no need to convert to min
    public int addTime(){
        mTime = mTime + 30;
        return mTime;
    }

    // If power is set at 1, change to 2 and vice versa
    public int switchPower(){
        if(mPower == 1){
            mPower = 2;
        }
        else if (mPower ==2){
            mPower =1;
        }
        return mPower;
    }
    // Start just prints the time and power the user set to
    public String start(){
        String settings = "Cooking for " + mTime+ " seconds at power level " + mPower;
        return settings;
    }

    public void restart(){
        mPower = 1;
        mTime = 0;
    }

    /**Getters & Setters*/
    public int getmTime() {
        return mTime;
    }

    public void setmTime(int mTime) {
        this.mTime = mTime;
    }

    public int getmPower() {
        return mPower;
    }

    public void setmPower(int mPower) {
        this.mPower = mPower;
    }
}

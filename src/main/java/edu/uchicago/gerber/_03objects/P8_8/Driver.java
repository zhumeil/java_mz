package edu.uchicago.gerber._03objects.P8_8;

import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P8.8 Voting Machine
 * */

public class Driver {


    public static void main(String[] args) {

        // List the options for actions
        Scanner in = new Scanner(System.in);
        System.out.println("VOTING MACHINE:\n" +
                "[1] Demorat\n" +
                "[2] Republican\n" +
                "[3] Clear\n" +
                "[4] Tally");

        // create object
        VotingMachine vm = new VotingMachine(0, 0);
        // while voting is in progress, we can vote or clear until we want to tally
        // when tally is selected the loop ends and results are printed
        boolean voting = true;
        while (voting){
            int vote = in.nextInt();
            switch (vote){
                case 1: vm.voteDem();
                        System.out.println("Democrat +1");
                        break;
                case 2: vm.voteRep();
                        System.out.println("Republican +1");
                        break;
                case 3: vm.clear();
                        break;
                case 4: System.out.println(vm.tally());
                        voting = false;
                        break;
            }
        }

    }
}

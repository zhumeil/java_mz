package edu.uchicago.gerber._03objects.P8_8;

public class VotingMachine {

    private int aDemocrat;
    private int aRepublican;

    // initialize object with vote options
    public VotingMachine(int democrat, int republican){
        aDemocrat = democrat;
        aRepublican = republican;
    }
    // increment vote
    public int voteDem (){
        aDemocrat = aDemocrat + 1;
        return aDemocrat;
    }
    // increment vote
    public int voteRep (){
        aRepublican = aRepublican + 1;
        return aRepublican;
    }
    // reset vote
    public void clear(){
        aRepublican = 0;
        aDemocrat = 0;
    }

    public String tally(){
        return "Democrats: " +  aDemocrat  + "\nRepublican: " + aRepublican;
    }

    /**Getters & Setters*/
    public int getaDemocrat() {
        return aDemocrat;
    }

    public void setaDemocrat(int aDemocrat) {
        this.aDemocrat = aDemocrat;
    }

    public int getaRepublican() {
        return aRepublican;
    }

    public void setaRepublican(int aRepublican) {
        this.aRepublican = aRepublican;
    }
}

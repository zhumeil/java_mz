package edu.uchicago.gerber._03objects.P8_6;

import java.util.Scanner;

/**
 * MPCS 51036
 * Meilin Zhu
 * P8.6 Car
 * */

public class Driver {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        // Get fuel efficiency
        System.out.println("Fuel efficiency: ");
        int fuelEfficiency = in.nextInt();
        // How much gas was added or to add
        System.out.println("Gas added:  ");
        int gas = in.nextInt();
        // Get miles driven or to drive
        System.out.println("Miles driven? ");
        int miles = in.nextInt();
        // Create car object
        Car myHybrid = new Car(fuelEfficiency);
        // Call methods from car class
        myHybrid.addGas(gas);
        myHybrid.drive(miles);
        System.out.println(myHybrid.getGasLevel() + " gallons remaining ");
    }
}

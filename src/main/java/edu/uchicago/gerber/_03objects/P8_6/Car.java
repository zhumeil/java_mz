package edu.uchicago.gerber._03objects.P8_6;

public class Car {

    private int gasLevel = 0;
    private int efficiency;

    // Initializing object with value for fuel efficieny
    public Car (int eff){
        efficiency = eff;
    }

    // Add add from user input
    public void addGas(int f){
        gasLevel = gasLevel + f;
    }

    // Find remaining gas level after driving
    public void drive(int miles){
        gasLevel = gasLevel - (miles/efficiency);
    }

    /**Getters & Setters*/
    public int getGasLevel() {
        return gasLevel;
    }

    public void setGasLevel(int gasLevel) {
        this.gasLevel = gasLevel;
    }

    public int getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(int efficiency) {
        this.efficiency = efficiency;
    }
}

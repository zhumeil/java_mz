package edu.uchicago.gerber._03objects.P8_19;

public class Cannonball {

    private double xP, yP;
    private double xV, yV;
    private double velocity;
    private double distance;
    // gravity is constant
    private final double GRAVITY = -9.81;

    // constructor to initialize x & y positions
    public Cannonball(double x){
        xP = x;
        yP = 0;
    }

    public void move(double sec){
        // calculate distance
        distance = sec * velocity;
        // calculate x & y positions
        xP = xV * sec;
        yP = yV * sec;
        // calculate y velocity
        yV = yV + GRAVITY * sec;
    }

    public void shoot( double angle, double velocity){
        // Find x & y velocities
        // Learned that you have to convert value to radians first
        // https://www.geeksforgeeks.org/java-math-cos-method-examples/
        xV = velocity * Math.cos(Math.toRadians(angle));
        yV = velocity * Math.sin(Math.toRadians(angle));
        // define time interval
        double time = 0.1;
        // call move until y-position is 0
        while (yP >= 0){
            move(time);
            time = time +0.1;
            if(yP< 0){
                break;
            }
            System.out.println("\nX :" + String.format("%.2f",getxP()));
            System.out.println("y: " + String.format("%.2f",getyP()));
        }
    }

    /**Getters & Setters*/
    public double getxP() {
        return xP;
    }

    public void setxP(double xP) {
        this.xP = xP;
    }

    public double getyP() {
        return yP;
    }

    public void setyP(double yP) {
        this.yP = yP;
    }

    public double getxV() {
        return xV;
    }

    public void setxV(double xV) {
        this.xV = xV;
    }

    public double getyV() {
        return yV;
    }

    public void setyV(double yV) {
        this.yV = yV;
    }
}

package edu.uchicago.gerber._03objects.P8_19;

import java.util.Scanner;
/**
 * MPCS 51036
 * Meilin Zhu
 * P8.19 Cannonball
 * */
public class Driver {
    public static void main(String[] args) {

        double angle;
        double velocity;
        // create new object
        Cannonball cb = new Cannonball(0);

        // get user input for angle and velocity
        Scanner in = new Scanner(System.in);
        System.out.println("ANGLE: ");
        angle = in.nextDouble();

        System.out.println("VELOCITY: ");
        velocity = in.nextDouble();

        // call shoot method
        cb.shoot(angle, velocity);


    }


}

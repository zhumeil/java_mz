package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class UFO extends Sprite{

    private Falcon falcon;

    public UFO(Falcon falcon) {
        super();
        this.falcon = falcon;
        setTeam(Movable.Team.FOE);

        setSpin(somePosNegValue(5));
        setDeltaX(somePosNegValue(15));
        setDeltaY(somePosNegValue(15));

        setExpiry(300);
        setRadius(70);
        setCenter(new Point(Game.R.nextInt(Game.DIM.width),
                Game.R.nextInt(Game.DIM.height)));
        setOrientation(Game.R.nextInt(360));
        setColor(Color.red);

        List<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(0,2));
        pntCs.add(new Point(1,1));
        pntCs.add(new Point(2,2));
        pntCs.add(new Point(4,0));
        pntCs.add(new Point(2,1));
        pntCs.add(new Point(0,-1));
        pntCs.add(new Point(-2,1));
        pntCs.add(new Point(-4,0));
        pntCs.add(new Point(-2,2));
        pntCs.add(new Point(-1,1));

        setCartesians(pntCs);

    }

    // UFO tracks Falcon
    // Based on professor's implementation
    @Override
    public void move() {
        super.move();
        setOrientation(getOrientation() + getSpin());
        expire();

        int dX = falcon.getCenter().x - this.getCenter().x;
        int dY = falcon.getCenter().y - this.getCenter().y;

        double radians = Math.atan2(dY, dX);
        setDeltaX(Math.cos(radians) * 3);
        setDeltaY(Math.sin(radians) * 3);
    }

    }


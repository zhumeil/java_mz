package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Shield extends Sprite{

    public Shield() {
        super();
        setTeam(Movable.Team.FLOATER);
        setSpin(somePosNegValue(10));
        setDeltaX(somePosNegValue(10));
        setDeltaY(somePosNegValue(10));

        setExpiry(250);
        setRadius(40);
        setCenter(new Point(Game.R.nextInt(Game.DIM.width),
                Game.R.nextInt(Game.DIM.height)));
        setOrientation(Game.R.nextInt(360));
        setColor(Color.cyan);

        List<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(0, 2));
        pntCs.add(new Point(1,1));
        pntCs.add(new Point(2, 0));
        pntCs.add(new Point(1,-1));
        pntCs.add(new Point(0,0));
        pntCs.add(new Point(-1, -1));
        pntCs.add(new Point(-2,0));
        pntCs.add(new Point(-1, 1));
        setCartesians(pntCs);
    }

    @Override
    public void move(){
        super.move();
        expire();
        setOrientation(getOrientation() + getSpin());
    }

}

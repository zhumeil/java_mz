package edu.uchicago.gerber._08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Debris extends Sprite {


    public Debris(Asteroid asteroid) {
        super();
        setExpiry(20);
        setTeam(Team.DEBRIS);

        setOrientation(asteroid.getOrientation());
        setRadius(getRadius());
        setCenter(asteroid.getCenter());

        setSpin(somePosNegValue(22));
        setDeltaX(somePosNegValue(22));
        setDeltaY(somePosNegValue( 22));

        List<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(1, 2));
        pntCs.add(new Point(2, 0));
        pntCs.add(new Point(1, -2));
        pntCs.add(new Point(-1, -2));
        pntCs.add(new Point(-2, 0));
        pntCs.add(new Point(-1, 2));
        setCartesians(pntCs);
    }

    public int getRadius(){
        Random r = new Random();
        return r.nextInt(8);
    }


    @Override
    public void move(){
        super.move();
        expire();
        setOrientation(getOrientation() + getSpin());
    }

}

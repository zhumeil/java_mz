package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class NewShipFloater extends Sprite {


	public NewShipFloater() {

		super();
		setTeam(Team.FLOATER);

		setExpiry(250);
		setRadius(30);
		setColor(Color.PINK);


		//set random DeltaX
		setDeltaX(somePosNegValue(10));

		//set rnadom DeltaY
		setDeltaY(somePosNegValue(10));
		
		//set random spin
		setSpin(somePosNegValue(10));

		//random point on the screen
		setCenter(new Point(Game.R.nextInt(Game.DIM.width),
				Game.R.nextInt(Game.DIM.height)));

		//random orientation 
		setOrientation(Game.R.nextInt(360));

		//always set cartesian points last
		List<Point> pntCs = new ArrayList<Point>();
		pntCs.add(new Point(0, 2));
		pntCs.add(new Point(1,1));
		pntCs.add(new Point(2, 0));
		pntCs.add(new Point(1,-1));
		pntCs.add(new Point(0,0));
		pntCs.add(new Point(-1, -1));
		pntCs.add(new Point(-2,0));
		pntCs.add(new Point(-1, 1));

		setCartesians(pntCs);
	}

	@Override
	public void move() {
		super.move();
		setOrientation(getOrientation() + getSpin());
		expire();

	}


}

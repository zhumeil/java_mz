package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;

// Using professor's explosion implementation as a weapon
public class SonicWave extends MoveAdaptor{

    private Point center;
    private int radius;
    private int expiry;
    private double deltaX, deltaY;


    public SonicWave(Falcon fal) {
        super();
        center = fal.getCenter();
        radius = 20;
        expiry = 20;
        deltaX = fal.getDeltaX();
        deltaY = fal.getDeltaY();

    }

    @Override
    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    @Override
    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getExpiry() {
        return expiry;
    }

    @Override
    public void draw(Graphics g){
            super.draw(g);
            g.setColor(Color.magenta);
            g.drawOval(center.x, center.y, getRadius(), getRadius());
    }

    @Override
    public void move() {
        Point pt = getCenter();
        double dX = pt.x + deltaX;
        double dY = pt.y + deltaY;

        if(pt.x > Game.DIM.width){
            setCenter(new Point(1, pt.y));
        }else if (pt.x < 0){
            setCenter(new Point(Game.DIM.width - 1, pt.y));
        }else if (pt.y > Game.DIM.height){
            setCenter(new Point(pt.x, 1));
        }else {
            setCenter(new Point((int)dX, (int)dY));
        }

        if(expiry > 0){
            radius += 10;
            expiry --;
        }else{
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        }
    }

    @Override
    public Team getTeam(){
        return Team.FRIEND;
    }

}

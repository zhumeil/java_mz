package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class MissileFloater extends Sprite {

    public MissileFloater() {
        super();
        setTeam(Movable.Team.FLOATER);
        setSpin(somePosNegValue(10));
        setDeltaX(somePosNegValue(15));
        setDeltaY(somePosNegValue(15));

        setExpiry(100);
        setRadius(40);
        setCenter(new Point(Game.R.nextInt(Game.DIM.width),
                Game.R.nextInt(Game.DIM.height)));
        setOrientation(Game.R.nextInt(360));
        setColor(Color.orange);

        List<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(0, 3));
        pntCs.add(new Point(1, 2));
        pntCs.add(new Point(1, -1));
        pntCs.add(new Point(-1, -1));
        pntCs.add(new Point(-1, 2));
        setCartesians(pntCs);
    }

    @Override
    public void move(){
        super.move();
        setOrientation(getOrientation() + getSpin());
        expire();
    }
}

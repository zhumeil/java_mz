package edu.uchicago.gerber._08final.mvc.model;

import edu.uchicago.gerber._08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SonicFloater extends Sprite {

    public SonicFloater(){
        super();
        setTeam(Movable.Team.FLOATER);
        setSpin(somePosNegValue(10));
        setDeltaX(somePosNegValue(20));
        setDeltaY(somePosNegValue(20));

        setExpiry(250);
        setRadius(30);
        setCenter(new Point(Game.R.nextInt(Game.DIM.width),
                Game.R.nextInt(Game.DIM.height)));
        setOrientation(Game.R.nextInt(360));
        setColor(Color.magenta);

        //make sure to setCartesianPoints last
        //defined the points on a cartesian grid
        List<Point> pntCs = new ArrayList<>();
        pntCs.add(new Point(0,4));
        pntCs.add(new Point(1,1));
        pntCs.add(new Point(4,0));
        pntCs.add(new Point(1,-1));
        pntCs.add(new Point(0,-4));
        pntCs.add(new Point(-1,-1));
        pntCs.add(new Point(-4,0));
        pntCs.add(new Point(-1,1));
        setCartesians(pntCs);


    }

    @Override
    public void move() {
        super.move();
        setOrientation(getOrientation() + getSpin());
        expire();

    }

}

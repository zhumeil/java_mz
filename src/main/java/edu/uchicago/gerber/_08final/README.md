# Java Asteroids

MPCS Final Project - Meilin Zhu

Used the base code provided and extend the Asteroids model to create my own version of Asteroids.

Below are the new features added


### Debris
When small asteroids are hit, debris is created. This does not harm Falcon, simply a visual indication that small asteroid has been destroyed.

### Score

UFO/Swarm = 100

Big Asteroid = 75 

Medium Asteroid = 50

Small Asteroid = 25


Once asteroids have been cleared from the screen, game enters a new level with increased speed. 


### Missile 
[G: to use]
Essentially a mega sized bullet, obtained through missile floaters. Has a larger radius and a longer expiration period.

### Sonic Wave
[F: to use] Creates sonic waves with a large range of destruction, obtained through sonic floaters

### UFO
Red enemy ship that follows Falcon

### Swarm
Group of smaller UFOs that travel faster and tracks the Falcon

### NewShipFloater
Add 1 life to Falcon, unlimited amount of lives can be accumulated

### Shield
Provides temporary protection against foes. Falcon color turns dark blue. Obtained through shield floaters

### Hyperspace
Instantly transports falcon to another location, obtained through hyperspace floaters